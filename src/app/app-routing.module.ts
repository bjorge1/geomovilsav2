import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  //{ path: 'home', loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)},
  { path: 'menu', loadChildren: './pages/menu/menu.module#MenuPageModule' },
  { path: 'login', loadChildren: './pages/login/login.module#LoginPageModule' },
  { path: 'fotos/:origin', loadChildren: './pages/fotos/fotos.module#FotosPageModule' },
  { path: 'foto', loadChildren: './pages/foto/foto.module#FotoPageModule' },
  { path: 'actividades', loadChildren: './pages/actividades/actividades.module#ActividadesPageModule' },
  { path: 'salida/:id', loadChildren: './pages/salida/salida.module#SalidaPageModule' },
  { path: 'ubicacion', loadChildren: './pages/ubicacion/ubicacion.module#UbicacionPageModule' },
  { path: 'observaciones', loadChildren: './pages/observaciones/observaciones.module#ObservacionesPageModule' },
  { path: 'nueva-qocha', loadChildren: './pages/nueva-qocha/nueva-qocha.module#NuevaQochaPageModule' },  { path: 'leyenda', loadChildren: './pages/leyenda/leyenda.module#LeyendaPageModule' },
  { path: 'lista', loadChildren: './pages/lista/lista.module#ListaPageModule' },
  { path: 'selector', loadChildren: './pages/selector/selector.module#SelectorPageModule' },
  { path: 'lista-detalles', loadChildren: './pages/lista-detalles/lista-detalles.module#ListaDetallesPageModule' },



];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
