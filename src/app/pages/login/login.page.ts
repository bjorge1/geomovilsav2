import { Component, OnInit, OnDestroy } from '@angular/core';
import { Plugins, NetworkStatus, PluginListenerHandle } from "@capacitor/core";
import { NavController, ToastController } from '@ionic/angular';
import * as CryptoJS from 'crypto-js';
import 'leaflet-draw';

const { Network} = Plugins;

import { UsuarioService, DatabaseService, DataService } from "../../providers/index.services";
import { Observable } from 'rxjs';
import { NativeStorage } from '@ionic-native/native-storage/ngx';


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit, OnDestroy{
  backgrounds = [
    'assets/icon/back.jpg',
    'assets/icon/back2.jpg'
  ];
  networkStatus: NetworkStatus;
  networkListener: PluginListenerHandle;
  user: string = "";
  password: string = "";
  modo: string = "on";
  private secureKey: string;
  private secureIV: string;
  private CryptoJS: any;
  items: any;

  constructor(public navCtrl: NavController,
              private usuarioservice: UsuarioService,
              private nativeStorage: NativeStorage,
              private toastCtrl: ToastController,
              private db: DatabaseService,
              private dataservice: DataService) {

  }
  async ngOnInit(){
    this.networkListener = Network.addListener(
      'networkStatusChange',
      status => {
      console.log('Network status changed', status);
      this.networkStatus = status;
      //console.log(status);
      }
    );
    this.networkStatus = await Network.getStatus();


  //   this.db.getDatabaseState().subscribe(ready => {
  //    if (ready) {
  //      this.db.getUsuarios().subscribe(users => {
  //        this.usuarios = users;
  //      })

  //    }
  //  });
  }


  ngOnDestroy(): void{
    this.networkListener.remove();
  }

  saveUser(tipoUser, idUser) {
    const myUser = this.user;
    const myString   = this.password;
    const myTipoUser = tipoUser;
    const myIdUser = idUser;
    const myPassword = 'sierrAzul@@@@';
    const encrypted = CryptoJS.AES.encrypt(myString, myPassword);
    const encrypteddb = encrypted.toString();
    console.log(encrypteddb);
    const decrypted = CryptoJS.AES.decrypt(encrypteddb, myPassword);
    console.log('Texto plano: ' + decrypted.toString(CryptoJS.enc.Utf8));
    this.nativeStorage.setItem('usuario',
        {usuario: myUser,
         password: encrypteddb,
         tipoUsuario: myTipoUser,
         idUsuario: myIdUser
        }
      )
      .then(
      async (data) => {
        console.log(data);
        const toast = await this.toastCtrl.create({
        message: 'Usuario almacenado correctamente.',
        duration: 2000
        });
        toast.present();
        console.log('Usuario guardado!', data); },
      error => console.error('Error storing item', error)
      );
  }

  iniciar_session() {
      const myPassword = 'sierrAzul@@@@';
      if( this.modo === 'on' ) {
        this.usuarioservice.autenticacion( this.user, this.password)
        .subscribe( () => {
            if (this.usuarioservice.ok) {
              this.usuarioservice.modoLogin('on');
              this.navCtrl.navigateRoot( '/menu/main' );
              this.saveUser(this.usuarioservice.tipo_usuario, this.usuarioservice.id_user);
              this.dataservice.setIdusuario(this.usuarioservice.id_user);
          }
       });
      } else {
        this.usuarioservice.modoLogin('off');
        this.nativeStorage.getItem('usuario')
        .then(
          async data => {
            this.items = data;
            console.log(this.items);
            console.log('Usuario: ' + this.items.usuario);
            console.log('Pass: ' + this.items.password);
            console.log('Id: ' + this.items.idUsuario);
            const decrypted = CryptoJS.AES.decrypt(this.items.password, myPassword);
            if (this.user === this.items.usuario && this.password === decrypted.toString(CryptoJS.enc.Utf8)) {
              this.usuarioservice.id_user = this.items.idUsuario;
              this.usuarioservice.tipo_usuario = this.items.tipoUsuario;
              this.navCtrl.navigateRoot( '/menu/main' );
              this.usuarioservice.modoLogin('off');
            } else {
              const toast = await this.toastCtrl.create({
                message: 'Usuario Incorrecto',
                duration: 2000
                });
              toast.present();
            }
          },
          error => console.error(error)
        );
    }
  }

}
