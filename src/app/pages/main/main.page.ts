import {
  Component,
  ViewChild,
  ElementRef,
  OnInit,
  OnDestroy,
  AfterViewInit,
  ComponentFactoryResolver
} from "@angular/core";
import {
  NavController,
  ToastController,
  Platform,
  ModalController,
  AlertController
} from "@ionic/angular";
//import * as fs from 'fs'
//import 'leaflet-tilelayer-mbtiles-ts';
import * as leaflet from "leaflet";
import "leaflet.locatecontrol";
import "leaflet-search";
import "leaflet-tilelayer-mbtiles-ts";
import { HttpClient } from "@angular/common/http";
import {
  FileTransfer,
  FileUploadOptions,
  FileTransferObject
} from "@ionic-native/file-transfer/ngx";
import { File } from "@ionic-native/file/ngx";
import { NativeStorage } from "@ionic-native/native-storage/ngx";
import { AndroidPermissions } from "@ionic-native/android-permissions/ngx";
import { LocationAccuracy } from "@ionic-native/location-accuracy/ngx";
import { Geolocation } from "@ionic-native/geolocation/ngx";
import { UsuarioService, DataService } from "src/app/providers/index.services";
import { DescargarMapaPage } from "../descargar-mapa/descargar-mapa.page";
import { NuevaQochaPage } from "../nueva-qocha/nueva-qocha.page";
import { PhotoViewer } from "@ionic-native/photo-viewer/ngx";

declare const L: any;
import "leaflet-draw";
import { LeyendaPage } from "../leyenda/leyenda.page";

@Component({
  selector: "app-main",
  templateUrl: "./main.page.html",
  styleUrls: ["./main.page.scss"]
})
export class MainPage implements OnInit {
  @ViewChild("map", { static: true }) mapContainer: ElementRef;
  map: any;
  data: any;
  json;
  estado: any;
  items: any;
  center = [];
  itemz: any;
  hidden1 = true;
  hidden2 = false;
  show_longitud: number = 0;
  show_latitud: number = 0;
  public myimage = "";
  capaAcciones = new L.GeoJSON();
  overlays2: { Estados: any };
  group: any;
  coordenadas: any;
  marker: any;
  backButtonSubscription;
  circle: any;
  srcMap = "file:///storage/emulated/0/UEFSA/Mbtiles/data.mbtiles";
  constructor(
    public navCtrl: NavController,
    public toastCtrl: ToastController,
    public http: HttpClient,
    public file: File,
    public transfer: FileTransfer,
    private nativeStorage: NativeStorage,
    private androidPermissions: AndroidPermissions,
    private locationAccuracy: LocationAccuracy,
    private geolocation: Geolocation,
    private usuarioservice: UsuarioService,
    public dataservice: DataService,
    private modalCtrl: ModalController,
    public platform: Platform,
    public alertController: AlertController
  ) {
    // this.presentToast();
    console.log(this.dataservice.getIdusuario());
  }

  ionViewDidEnter() {
    if (this.usuarioservice.modo() === "on") {
      this.loadMb();
    } else {
      this.nativeStorage.getItem("center").then(data => {
        if (data.x !== '') {
          this.loadMb_second();
        } else {
          this.saveCenter();
          this.loadMb_second();
        }
      },
      error => console.log("Item no encontrado " + error));
    }
  }
  saveCenter() {
    var E = 0;
    var O = 0;
    var caja = 0;
    // console.log(E, O);
    this.nativeStorage
      .setItem("center", {
        ejeX: E,
        ejeY: O,
        box: caja
      })
      .then(
        data => {
          console.log(data);
          this.loadMb_second();
        },
        error => console.error("Error storing item", error)
      );
  }

  ionViewDidLeave() {
    this.map = null;
    console.log("clear");
  }

  ngOnInit() {}

  async presentToast() {
    console.log("TOAST");
    const toast = await this.toastCtrl.create({
      message: "Bienvenido...",
      duration: 3000
    });
    toast.present();
  }

  addEvent() {
    if (this.myimage === "") {
      this.myimage = "assets/gifs/loading.svg";
    } else {
      this.myimage = "assets/gifs/loading.svg";
    }
  }

  async openLeyenda() {
    console.log("leyenda");
    const modal = await this.modalCtrl.create({
      component: LeyendaPage,
      cssClass: "my-custom-modal-css"
    });
    return await modal.present();
  }

  loadMb() {
    this.center = [-12.0744, -77.0437]; // Huancayo
    this.map = L.map("map", {
      center: this.center,
      zoom: 16
    });
    this.map.attributionControl.setPrefix("GeoColector Online");
    this.group = L.control.layers().addTo(this.map);
    let mb = L.tileLayer(
      "http://mt1.google.com/vt/lyrs=y&hl=en&x={x}&y={y}&z={z}",
      {
        // attribution: "Google Satélite",
        maxNativeZoom: 18,
        maxZoom: 22
      }
    ).addTo(this.map);
    let mb2 = L.tileLayer(
      "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
      {
        // attribution: "Open Streem Maps",
        maxNativeZoom: 18,
        maxZoom: 22
      }
    ).addTo(this.map);
    this.group.addBaseLayer(mb, "🏙 Google Satelite");
    this.group.addBaseLayer(mb2, "🗺️ Open Street Maps");

    this.loadStorage();
    this.loadProyectos();
  }

  loadMb_second() {
    this.hidden1 = false;
    // this.srcMap = this.dataservice.directory;
    this.nativeStorage.getItem("srcMap").then(data => {
      if (data.src !== '') {
        this.srcMap = data.src;
      } else {
        console.log(this.srcMap);
      }
      console.log(this.srcMap);
    },
    error => console.log("Item no encontrado " + error));
    console.log(this.srcMap);
    this.nativeStorage.getItem("center").then(
      data => {
        this.coordenadas = data;
        console.log(this.coordenadas);
        this.center = [this.coordenadas.ejeY, this.coordenadas.ejeX]; // HUANCAYO
        console.log(this.center);
        this.map = L.map("map", {
          // attributionControl: false,
          center: this.center,
          zoom: 16
        });
        this.map.attributionControl.setPrefix("GeoColector Offline");
        this.group = L.control.layers().addTo(this.map);
        let win: any = window;
        let safeURL = win.Ionic.WebView.convertFileSrc(this.srcMap);
        // this.file.resolveLocalFilesystemUrl(path)
        console.log(safeURL);
        let mb = L.tileLayer
          .mbTiles(safeURL, {
            // attribution: "Mapa Base",
            maxNativeZoom: 19,
            maxZoom: 20,
            minZoom: 0
          })
          .addTo(this.map);
        this.group.addBaseLayer(mb, "Mapa base");
        mb.on("databaseloaded", function(ev) {
          console.log("MBTiles DB loaded", ev);
        });
        mb.on("databaseerror", function(ev) {
          console.log("MBTiles DB error", ev);
        });
        this.loadStorage();
        this.loadProyectos();
      },
      error => console.log("Item no encontrado " + error)
    );
  }

  loadStorage() {
    var myIcon = L.icon({
      iconUrl: "assets/icon/red.png",
      iconSize: [32, 37],
      iconAnchor: [16, 37],
      popupAnchor: [0, -28]
    });
    this.nativeStorage.getItem("acciones").then(
      data => {
        this.items = data;
        var user = this.dataservice.id_usuario;
        var cnxn = this.usuarioservice.modo();
        const capa = L.geoJSON(this.items, {
          pointToLayer(feature, layer) {
            return L.marker(layer, { icon: myIcon });
          },
          onEachFeature(feature, layer) {
            layer
              .bindPopup(
                "<h2>" +
                  feature.properties.cod_obra +
                  "</h2><p>Nombre: " +
                  feature.properties.nomb_lugar_intervenc +
                  '</p><a href ="/salida/' +
                  feature.properties.cod_obra +
                  "," +
                  user +
                  "," +
                  cnxn +
                  '" >Capturar</a>'
              )
          }
        }).addTo(this.map);
        new L.Control.Search({
          layer: capa,
          textPlaceholder: "Buscar Acciones...",
          propertyName: "cod_obra"
        }).addTo(this.map);
        this.group.addOverlay(capa, "Acciones");
      },
      error => console.error(error)
    );
  }

  loadProyectos() {
    this.nativeStorage.getItem("proyectos").then(
      data => {
        var user = this.dataservice.id_usuario;
        var cnxn = this.usuarioservice.modo();
        this.items = data;
        const capa = L.geoJSON(this.items, {
          onEachFeature(feature, layer) {
            layer
              .on("click", () => this.openFeature())
              .bindPopup(
                "<h2> Inversión: " +
                  feature.properties.cod_inversion +
                  "</h2>" +
                  "<p>Unidad : " +
                  feature.properties.cod_unid_produc +
                  "</p>" +
                  "<p>Año: " +
                  feature.properties.ano_formulacion +
                  "</p>" +
                  '<a href ="/salida/' +
                  feature.properties.cod_inversion +
                  "," +
                  user +
                  "," +
                  cnxn +
                  '" >Capturar</a>'
              );
          }
        }).addTo(this.map);
        new L.Control.Search({
          layer: capa,
          textPlaceholder: "Buscar Inversiones...",
          propertyName: "cod_inversion",
          zoom: 15
        }).addTo(this.map);
        this.group.addOverlay(capa, "Proyectos");
      },
      error => console.error(error)
    );
  }

  loadDistritos() {
    const myStyle = { color: "#33B8FF", fillOpacity: 0 };
    this.nativeStorage.getItem("distritos").then(
      data => {
        this.items = data;
        const capa = L.geoJSON(this.items, {
          onEachFeature(feature, layer) {
            layer
              .bindTooltip(feature.properties.nombdist, {
                direction: "center",
                permanent: true,
                className: "labelstyle"
              })
              .openTooltip();
          },
          style: myStyle
        }).addTo(this.map);
        new L.Control.Search({
          layer: capa,
          textPlaceholder: "Buscar Distrito...",
          propertyName: "nombdist"
        }).addTo(this.map);
        this.group.addOverlay(capa, "Distritos");
        console.log("Distritos cargados");
      },
      error => console.error(error)
    );
  }

  loadComunidades() {
    const myStyle = { color: "#b53333", fillOpacity: 0 };
    this.nativeStorage.getItem("comunidades").then(
      data => {
        this.items = data;
        const capa = L.geoJSON(this.items, {
          onEachFeature(feature, layer) {
            layer
              .bindTooltip(feature.properties.nom_comu_c, {
                direction: "center",
                permanent: true,
                className: "labelstyle"
              })
              .openTooltip();
          },
          style: myStyle
        }).addTo(this.map);
        new L.Control.Search({
          layer: capa,
          textPlaceholder: "Buscar Comunidad...",
          propertyName: "nom_comu_c"
        }).addTo(this.map);
        this.group.addOverlay(capa, "Comunidades");
      },
      error => console.error(error)
    );
  }

  actualLocation() {
    var marker;
    this.map
      .locate({
        setView: true,
        maxZoom: 120
      })
      .on("locationfound", e => {
        if (marker == undefined) {
          marker = new L.marker(e.latlng).addTo(this.map);
        } else {
          marker.setLatLng(e.latlng);
        }
      })
      .on("locationerror", error => {
        if (marker) {
          this.map.removeLayer(marker);
          marker = undefined;
        }
      });
  }

  getGeolocation() {
    // if(this.map === undefined){
    //   var subheader = "No se pudo obtener la ubicación actual";
    //   var mensaje =
    //     "Por favor verifique si dió los permisos necesarios o si tiene la version offline del mapa al que intenta acceder.";
    //   this.presentAlert(subheader, mensaje);
    // } else {
    this.geolocation
      .getCurrentPosition({
        maximumAge: 0,
        timeout: 20000,
        enableHighAccuracy: true
      })
      .then(resp => {
        const precisión = resp.coords.accuracy;
        const accuracy = resp.coords.accuracy.toFixed(2);
        this.show_latitud = resp.coords.latitude;
        this.show_longitud = resp.coords.longitude;
        console.log(this.show_latitud);
        console.log(this.show_longitud);
        console.log(accuracy);
        if (precisión < 100) {
          if (this.marker === undefined && this.circle === undefined) {
            this.marker = L.marker([this.show_latitud, this.show_longitud]);
            this.circle = L.circleMarker(
              [this.show_latitud, this.show_longitud],
              precisión
            );
            this.dataservice.setLatLong(this.show_latitud, this.show_longitud);
          } else {
            this.circle.setLatLng([this.show_latitud, this.show_longitud]);
            this.circle.setRadius(precisión);
            this.marker.setLatLng([this.show_latitud, this.show_longitud]);
            this.dataservice.setLatLong(this.show_latitud, this.show_longitud);
          }
          this.circle.addTo(this.map);
          this.marker
            .on("click", () => this.openModal())
            .addTo(this.map)
            .bindPopup(
              "Precisión actual : " +
                accuracy +
                " m. <br> Haga clic en el marcador para crear un nuevo registro."
            )
            .openPopup();
          this.map.flyTo([this.show_latitud, this.show_longitud], 16);
        } else {
          var subheader = "Precisión GPS Alta";
          var mensaje =
            "Su precisión actual es la de " +
            accuracy +
            " m. Por favor capture otra vez.";
          this.presentAlert(subheader, mensaje);
        }
      })
      .catch(error => {
        console.log(error);
        var subheader = "No se pudo obtener la ubicación actual";
        var mensaje =
          "Por favor verifique si dió los permisos necesarios o si tiene la version offline del mapa al que intenta acceder.";
        this.presentAlert(subheader, mensaje);
      });
  }
  // }

  async presentAlert(subheader, mensaje) {
    const alert = await this.alertController.create({
      header: "Advertencia",
      subHeader: subheader,
      message: mensaje,
      buttons: ["OK"]
    });
    await alert.present();
  }

  openFeature(id) {
    this.dataservice.setIdQocha(id);
    this.navCtrl.navigateForward("/salida");
  }
  openModal() {
    this.navCtrl.navigateForward("/nueva-qocha");
    console.log("Open modal");
  }

  // Check if application having GPS access permission
  checkGPSPermission() {
    this.androidPermissions
      .checkPermission(
        this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION
      )
      .then(
        result => {
          if (result.hasPermission) {
            // If having permission show 'Turn On GPS' dialogue
            this.askToTurnOnGPS();
          } else {
            // If not having permission ask for permission
            this.requestGPSPermission();
          }
        },
        err => {
          alert(err);
        }
      );
  }
  requestGPSPermission() {
    this.locationAccuracy.canRequest().then((canRequest: boolean) => {
      if (canRequest) {
        console.log("4");
      } else {
        // Show 'GPS Permission Request' dialogue
        this.androidPermissions
          .requestPermission(
            this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION
          )
          .then(
            () => {
              // call method to turn on GPS
              this.askToTurnOnGPS();
            },
            error => {
              alert("Error al solicitar permisos de ubicación.");
            }
          );
      }
    });
  }

  askToTurnOnGPS() {
    this.locationAccuracy
      .request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY)
      .then(
        () => {
          // When GPS Turned ON call method to get Accurate location coordinates
          this.getGeolocation();
        },
        error => {
          var subheader = "Permisos Denegados";
          var mensaje = "El usuario denegó la solicitud de encender el GPS";
          this.presentAlert(subheader, mensaje);
        }
      );
  }
}
