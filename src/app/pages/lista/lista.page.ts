import { Component, OnInit } from "@angular/core";
import { DatabaseService, DataService } from "../../providers/index.services";
import { BehaviorSubject } from 'rxjs';
import { NavController } from '@ionic/angular';
import { ListaDetallesPage } from '../lista-detalles/lista-detalles.page';

@Component({
  selector: "app-lista",
  templateUrl: "./lista.page.html",
  styleUrls: ["./lista.page.scss"]
})
export class ListaPage implements OnInit {
  salidas = new BehaviorSubject([]);
  nuevas = new BehaviorSubject([]);
  constructor(private navCtrl: NavController, private databaseService: DatabaseService, private dataService: DataService) {}

  ngOnInit() {
    this.databaseService.getSalidas();
    this.salidas = this.databaseService.salidas;
    this.databaseService.loadNuevas();
    this.databaseService.getNuevas();
    this.nuevas = this.databaseService.nuevas;
  }

  scrollTo(e) {
    e.srcElement.scrollIntoView({ behavior: 'smooth', block: 'start' });
  }

  openDetailsTbl(idTabla) {
    this.dataService.setIdTbl(idTabla);
    this.navCtrl.navigateForward('lista-detalles');
  }
  openDetailsTblNuevo(idSalida) {
    this.dataService.setIdTblPntNuevo(idSalida);
    this.navCtrl.navigateForward('lista-detalles');
  }
}
