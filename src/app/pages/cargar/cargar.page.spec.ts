import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CargarPage } from './cargar.page';

describe('CargarPage', () => {
  let component: CargarPage;
  let fixture: ComponentFixture<CargarPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CargarPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CargarPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
