import { Component, OnInit, Sanitizer } from '@angular/core';
import { FilePath } from '@ionic-native/file-path/ngx';
import { FileChooser } from '@ionic-native/file-chooser/ngx';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { LoadingController } from '@ionic/angular';
import { DatabaseService, CargarService } from "../../providers/index.services";
import { ActivatedRoute, Router } from '@angular/router';
import { File } from '@ionic-native/file/ngx';
import { Platform } from '@ionic/angular';

@Component({
  selector: 'app-cargar',
  templateUrl: './cargar.page.html',
  styleUrls: ['./cargar.page.scss'],
})
export class CargarPage implements OnInit {
  public myimage = '';
  returnPath:string = '';
  path: SafeResourceUrl;
  constructor(public filePath: FilePath, public sanitizer: DomSanitizer,
              public fileChooser: FileChooser, public loadingController: LoadingController,
              private databaseService: DatabaseService, private cargaService: CargarService,
              private router: Router, private file: File, private platform: Platform) {
               }

  ngOnInit() {
  }
  addEvent() {
    if (this.myimage === '') {
      this.presentLoadingWithOptions();
      this.myimage = 'assets/gifs/loading.svg';
    } else {
      this.myimage = '';
    }
  }

  cargar() {
    this.presentLoadingWithOptions();
    this.myimage = 'assets/gifs/loading.svg';
    this.databaseService.loadSalidas().then(() => {
      this.databaseService.loadFotos().then(() => {
        this.databaseService.loadFotosOnlyImage().then(() => {
          console.log(this.databaseService.onlyFotos);
          this.cargaService.sincronizarSalida(this.databaseService.salidas, this.databaseService.fotos )
          .then(() => {

            this.cargaService.sincronizarFotoonly(this.databaseService.onlyFotos).then(() => {
              this.myimage = '';
              this.loadingController.dismiss();
              this.databaseService.deleteSalidaFoto();
              this.databaseService.deleteSalida().then(() => {
              this.router.navigateByUrl('/');
              });
            });
          });
        });
      });
    });
  }

  cargarPuntos() {
    this.presentLoadingWithOptions();
    this.myimage = 'assets/gifs/loading.svg';
    this.databaseService.loadNuevas().then(() => {
      this.databaseService.loadNuevasFotos().then(() => {
        this.databaseService.loadFotosOnlyImageNuevo().then(() => {

          this.cargaService.sincronizarPunto(this.databaseService.nuevas, this.databaseService.nuevasFotos )
          .then(() => {
            this.cargaService.sincronizarPuntoFotoonly(this.databaseService.onlyNuevosFotos).then(() => {
              this.myimage = '';
              this.loadingController.dismiss();
              this.databaseService.deleteNuevoFoto();
              this.databaseService.deleteNuevo().then(() => {
              this.router.navigateByUrl('/');
              });
            });
          });
        });
      });
    });
  }

  pickFile() {
    this.fileChooser.open().then((fileuri) => {
      this.filePath.resolveNativePath(fileuri).then((resolvedNativePath) => {
        this.returnPath = resolvedNativePath;
        this.path = this.sanitizer.bypassSecurityTrustUrl(this.returnPath);
      });
    });
  }

  async presentLoadingWithOptions() {
    const loading = await this.loadingController.create({
      spinner: null,
      duration: 5000,
      message: 'Por favor espere...',
      translucent: true,
      cssClass: 'custom-class custom-loading'
    });
    return await loading.present();
  }


}