import { Component, OnInit, OnDestroy } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { DatePicker } from "@ionic-native/date-picker/ngx";
import { DatePipe } from "@angular/common";
import {
  Platform,
  NavController,
  ModalController,
  ToastController,
  LoadingController
} from "@ionic/angular";
import { HttpClient } from "@angular/common/http";
import {
  DataService,
  DatabaseService,
  UsuarioService
} from "../../providers/index.services";
import { NativeStorage } from "@ionic-native/native-storage/ngx";

@Component({
  selector: "app-salida",
  templateUrl: "./salida.page.html",
  styleUrls: ["./salida.page.scss"]
})
export class SalidaPage implements OnInit {
  fecha: string = "";
  count_observaciones: any;
  count_fotos: any;
  observations: any;
  fotos: any;
  id: any;
  data1: any = {};
  data2: any = {};
  map: any;
  confirmar: boolean = false;
  mostrar1: boolean = false;
  mostrar2: boolean = false;
  idusuario: any;
  mode: string;

  constructor(
    private datePicker: DatePicker,
    private datepipe: DatePipe,
    public platform: Platform,
    private navCtrl: NavController,
    public modalController: ModalController,
    public dataService: DataService,
    private activatedRoute: ActivatedRoute,
    public http: HttpClient,
    public toastCtrl: ToastController,
    public loadingController: LoadingController,
    private nativeStorage: NativeStorage,
    private databaseService: DatabaseService,
    private usuarioService: UsuarioService
  ) {
    this.platform.ready().then(() => {
      this.fecha = this.datepipe.transform(new Date(), "dd-MM-yyyy");

      this.databaseService.getDatabaseState().subscribe(rdy => {
        if (rdy) {
          //this.presentToast('Base de datos creada...')
          console.log("Base de datos creada...");
        }
      });
    });

    this.data1 = {
      cod_obra: "",
      nomb_lugar_intervenc: "",
      cod_provincia: "",
      cod_distrito: ""
    };

    this.data2 = {
      nomb_departamento: "",
      nombre_proyecto: "",
      cod_inversion: ""
    };
  }

  ngOnInit() {
    this.platform.ready().then(() => {
      /*this.activatedRoute.queryParams.subscribe(params => {
        const data = params['data'];
        console.log(data);
      });*/
      var str = this.activatedRoute.snapshot.paramMap.get("id");
      var splitted = str.split(",");
      this.id = splitted[0];
      this.idusuario = splitted[1];
      this.mode = splitted[2];

      console.log(this.id);
      console.log("id usuario salida--> " + this.idusuario);
      console.log("Modo conexión --> " + this.mode);
      this.dataService.id_salida = str;
      this.cargarGjson();
      //console.log(this.data)
      /*this.dataService.serviceData
        .subscribe(data => (this.observations = data));
        console.log("sent data from observaciones page : ", this.observations);
        console.log(this.observations);*/
      this.dataService.serviceDataColecion.subscribe(
        data => (this.fotos = data)
      );
      console.log("Datos obtenidos: ", this.fotos);
      //console.log(this.fotos);
    });
  }

  async presentToast(mensaje: any) {
    const toast = await this.toastCtrl.create({
      message: mensaje,
      duration: 2000
    });
    toast.present();
  }

  async saveData() {
    var str = this.activatedRoute.snapshot.paramMap.get('id');
    var splitted = str.split(',');
    this.id = splitted[0];
    this.idusuario = splitted[1];
    this.mode = splitted[2];
    this.presentLoadingWithOptions("Guardando...");
    this.usuarioService.modoLogin(this.mode);
    this.dataService.setIdusuario(this.idusuario);
    console.log("id usuario salida--> " + this.idusuario);
    if (this.id.indexOf('-') !== -1) {
      this.databaseService
        .addSalida(this.id, this.idusuario, "acciones", this.fotos)
        .then(() => {
          this.navCtrl.navigateRoot("/menu");
        });
    } else {
      this.databaseService
        .addSalida(this.id, this.idusuario, "proyectos", this.fotos)
        .then(() => {
          this.navCtrl.navigateRoot("/menu");
        });
    }

    /*if(this.id.indexOf('-') != -1){
      this.databaseService.addAccion("Hca4-2018-Q5", "01", this.fotos );
    }else{
      this.databaseService.addProyecto("2400688", "01", this.fotos );
    }*/
  }

  async presentLoadingWithOptions(mensaje: any) {
    const loading = await this.loadingController.create({
      spinner: "bubbles",
      duration: 5000,
      message: mensaje,
      translucent: true,
      cssClass: "custom-class custom-loading"
    });
    return await loading.present();
  }

  cargarGjson() {
    if (this.id.indexOf("-") != -1) {
      this.mostrar1 = false;
      this.mostrar2 = true;

      this.nativeStorage.getItem("acciones").then(
        data => {
          data.features.forEach(obj => {
            /*Object.entries(obj).forEach(([key, value]) => {
               console.log(`${key} ${value}`);
           });*/
            if (obj.properties.cod_obra === this.id) {
              this.data1 = obj.properties;

              console.log(this.data1);
            }
            //console.log('-------------------');
          });
        },
        error => console.error(error)
      );
    } else {
      this.mostrar1 = true;
      this.mostrar2 = false;
      this.nativeStorage.getItem("proyectos").then(
        data => {
          data.features.forEach(obj => {
            //console.log(obj.properties);
            /*Object.entries(obj).forEach(([key, value]) => {
               console.log(`${key} ${value}`);

           });*/
            if (obj.properties.cod_inversion == this.id) {
              this.data2 = obj.properties;

              console.log(this.data2);
            }
            //console.log('-------------------');
          });
        },
        error => console.error(error)
      );
    }
  }

  ionViewWillEnter() {
    /*console.log(this.observations);
    this.count_observaciones = this.observations.length;*/
    console.log(this.fotos.length);
    this.count_fotos = this.fotos.length;
  }

  show_date() {
    this.datePicker
      .show({
        date: new Date(),
        mode: "date",
        androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
      })
      .then(
        date => {
          console.log("Got date: ", date);
          this.fecha = this.datepipe.transform(date, "dd-MM-yyyy");
        },
        err => console.log("Error occurred while getting date: ", err)
      );
  }

  seleccionar() {
    this.navCtrl.navigateRoot("/ubicacion");
  }

  capturar() {
    this.navCtrl.navigateForward("/fotos/fromSalida");
  }
  getData(id: any) {
    console.log(id);
  }

  observaciones() {
    this.navCtrl.navigateForward("/observaciones");
    /*this.navCtrl.push(ObservacionesPage,{
      callback: this.callback
    });*/
  }

  goBack() {
    this.navCtrl.navigateRoot("/menu");
    this.usuarioService.modoLogin(this.mode);
    this.dataService.setIdusuario(this.idusuario);
  }

  ionViewWillLeave() {
    this.usuarioService.modoLogin(this.mode);
    this.dataService.setIdusuario(this.idusuario);
  }
}
