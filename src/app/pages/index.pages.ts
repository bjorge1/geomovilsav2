
export { CargarPage }  from "./cargar/cargar.page";
export { DescargarPage }  from "./descargar/descargar.page";
export { RespaldarPage } from "./respaldar/respaldar.page";
export { ObservacionesPage }  from "./observaciones/observaciones.page";

export { SalidaPage } from './salida/salida.page';
