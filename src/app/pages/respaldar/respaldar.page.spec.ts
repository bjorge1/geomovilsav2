import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RespaldarPage } from './respaldar.page';

describe('RespaldarPage', () => {
  let component: RespaldarPage;
  let fixture: ComponentFixture<RespaldarPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RespaldarPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RespaldarPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
