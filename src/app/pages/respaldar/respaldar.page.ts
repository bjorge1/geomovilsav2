import { Component, OnInit } from '@angular/core';
import { DatabaseService } from 'src/app/providers/index.services';

@Component({
  selector: 'app-respaldar',
  templateUrl: './respaldar.page.html',
  styleUrls: ['./respaldar.page.scss'],
})
export class RespaldarPage implements OnInit {

  constructor(private databaseService: DatabaseService) { }

  ngOnInit() {
  }


  exportDB() {
    this.databaseService.exportDbToSql();
  }

  importDB() {
    this.databaseService.importFileToSQL();
  }
}
