import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { MenuPage } from './menu.page';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/menu/main',
    pathMatch: 'full'
  },
  {
    path: '',
    component: MenuPage,
    children: [
      { path: 'main', loadChildren: '../main/main.module#MainPageModule' },
      { path: 'lista', loadChildren: '../lista/lista.module#ListaPageModule' },
      { path: 'cargar', loadChildren: '../cargar/cargar.module#CargarPageModule' },
      { path: 'descargar', loadChildren: '../descargar/descargar.module#DescargarPageModule' },
      { path: 'respaldar', loadChildren: '../respaldar/respaldar.module#RespaldarPageModule' },
      { path: 'selector', loadChildren: '../selector/selector.module#SelectorPageModule' },
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [MenuPage]
})
export class MenuPageModule {}
