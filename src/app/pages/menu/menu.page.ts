import { Component, OnInit } from '@angular/core';
import { UsuarioService } from 'src/app/providers/index.services';
import { Platform } from '@ionic/angular';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.page.html',
  styleUrls: ['./menu.page.scss'],
})
export class MenuPage implements OnInit {
  pages = [];
  backButtonSubscription;
  constructor(private usuarioservice: UsuarioService, private platform: Platform) { }

  ngOnInit() {
    if (this.usuarioservice.modo() === 'on' ) {
      this.pages = [
        {
          title: 'Principal',
          url: '/menu/main',
          icon: 'home'
        },
        {
          title: 'Mapas',
          url: '/menu/selector',
          icon: 'list-box'
        },
        {
          title: 'Listado',
          url: '/menu/lista',
          icon: 'list-box'
        },
        {
          title: 'Sincronización',
          children: [
            {
              title: 'Descargar',
              url: '/menu/descargar',
              icon: 'ios-cloud-download'
            },
            {
              title: 'Cargar',
              url: '/menu/cargar',
              icon: 'ios-cloud-upload'
            }
          ]
        },
        {
          title: 'Respaldar',
          url: '/menu/respaldar',
          icon: 'document'
        },
      ];
    } else {
      this.pages = [
        {
          title: 'Principal',
          url: '/menu/main',
          icon: 'home'
        },
        {
          title: 'Mapas',
          url: '/menu/selector',
          icon: 'list-box'
        },
        {
          title: 'Listado',
          url: '/menu/lista',
          icon: 'list-box'
        },
        {
          title: 'Respaldar',
          url: '/menu/respaldar',
          icon: 'document'
        }
      ];
    }


  }
  exit() {
    console.log('saliendoooo');
    navigator['app'].exitApp();
  }

}
