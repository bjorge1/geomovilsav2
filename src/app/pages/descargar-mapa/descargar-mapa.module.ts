import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DescargarMapaPage } from './descargar-mapa.page';

const routes: Routes = [
  {
    path: '',
    component: DescargarMapaPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [DescargarMapaPage]
})
export class DescargarMapaPageModule {}
