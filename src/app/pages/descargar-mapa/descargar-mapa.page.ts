import { Component, OnInit } from '@angular/core';
import { File } from '@ionic-native/file/ngx';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { ToastController } from '@ionic/angular';
import { UsuarioService, DescargarService } from 'src/app/providers/index.services';
import { URL_SERVICES } from 'src/app/config/url.services';
import { Http } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { NativeStorage } from '@ionic-native/native-storage/ngx';

export interface Box{
  cnombre: string;
  box: string;
  x: string;
  y: string;
  x2: string;
  y2: string;
  id_usuario: string;
  idbox: string;
  ccalidad: string;
}

@Component({
  selector: 'app-descargar-mapa',
  templateUrl: './descargar-mapa.page.html',
  styleUrls: ['./descargar-mapa.page.scss'],
})
export class DescargarMapaPage implements OnInit {
  hidden = true;
  disabled = false;
  fileTransfer: FileTransferObject;
  myimage = '';
  public boxes: Box[];
  items: any;
  public id: any;
  constructor(private file: File, private transfer: FileTransfer, private http: Http,
              private toastCtrl: ToastController, private usuarioservice: UsuarioService,
              private descargarService: DescargarService,
              private nativeStorage: NativeStorage) {

  }

  ngOnInit() {
    this.getCajas();
  }

  fileLoad(nombre, box) {
    this.disabled = true;
    this.hidden = false;
    let link = 'http://179.43.81.84:8080/geoserver/uefsa/wms?service=WMS&version=1.1.0&request=GetMap&layers=uefsa%3Aoffline&bbox='
                + box + '&width=531&height=768&srs=EPSG%3A4326&format=mbtiles&format_options=min_zoom:12;max_zoom:20';
    console.log(link);
    let path = '';
    let file_name = nombre + '.mbtiles';
    console.log(file_name);
    let dir_name = 'UEFSA/Mbtiles'; // directory to download - you can also create new directory
    this.file.createDir(this.file.externalRootDirectory, 'UEFSA', true);
    this.fileTransfer = this.transfer.create();
    let result = this.file.createDir(this.file.externalRootDirectory, dir_name, true);
    result.then((resp) => {
      path = resp.toURL();
      console.log(path);
      this.fileTransfer.download(link, path + file_name, true)
      .then(async (entry) => {
        const message = 'Mapa descargado correctamente.';
        this.descargarService.toast(message);
        this.myimage = '';
        this.disabled = false;
        this.hidden = true;
      }, (error) => {
        console.log(error);
      });
      this.fileTransfer.onProgress((progressEvent) => {
        if (progressEvent.lengthComputable) {
          var perc = progressEvent.loaded;
          console.log(perc);
        }
      })
    }, (err) => {
      console.log('error on creating path : ' + err);
    });
  }

  fileLoadLow(nombre, box) {
    this.disabled = true;
    this.hidden = false;
    let link = 'http://179.43.81.84:8080/geoserver/uefsa/wms?service=WMS&version=1.1.0&request=GetMap&layers=uefsa%3Aoffline&bbox='
                + box + '&width=531&height=768&srs=EPSG%3A4326&format=mbtiles&format_options=min_zoom:10;max_zoom:16';
    console.log(link);
    let path = '';
    let file_name = nombre + '.mbtiles';
    console.log(file_name);
    let dir_name = 'UEFSA/Mbtiles'; // directory to download - you can also create new directory
    this.file.createDir(this.file.externalRootDirectory, 'UEFSA', true);
    this.fileTransfer = this.transfer.create();
    let result = this.file.createDir(this.file.externalRootDirectory, dir_name, true);
    result.then((resp) => {
      path = resp.toURL();
      console.log(path);
      this.fileTransfer.download(link, path + file_name, true)
      .then(async (entry) => {
        const message = 'Mapa descargado correctamente.';
        this.descargarService.toast(message);
        this.myimage = '';
        this.disabled = false;
        this.hidden = true;
      }, (error) => {
        console.log(error);
      });
      this.fileTransfer.onProgress((progressEvent) => {
        if (progressEvent.lengthComputable) {
          var perc = progressEvent.loaded;
          console.log(perc);
        }
      })
    }, (err) => {
      console.log('error on creating path : ' + err);
    });
  }

  addEvent() {
    if (this.myimage === ''){
      this.myimage = 'assets/gifs/loading.svg';
    } else {
      this.myimage = 'assets/gifs/loading.svg';
    }
  }

  cancelFileLoad() {
    this.myimage = '';
    this.hidden = true;
    this.disabled = false;
    this.fileTransfer.abort();
  }

  getCajas() {
    this.nativeStorage.getItem('usuario')
      .then(
        async data => {
        this.items = data;
        this.id = this.items.idUsuario;
        let service = URL_SERVICES + '/box/select?idusuario=' + this.id;
        // + this.id;
        console.log(service);
        this.http.get(service)
          .subscribe (res => {
            let data = res.json();
            let box = data.data;
            this.boxes = box;
          }, err => {
            console.log(err);
          }
        );
      },
        error => console.error(error)
      );
    console.log(this.id);
  }

  saveCenter(x, y, x2, y2, box) {
    var E = (x + x2) / 2;
    var O = (y + y2) / 2;
    var caja = box;
    console.log(E, O);
    this.nativeStorage.setItem('center', {
      ejeX: E,
      ejeY: O,
      box: caja
    })
    .then(data => {
      console.log(data);
    }, error => console.error('Error storing item', error)
    );
  }

  deleteBox(idbox) {
    let service = URL_SERVICES + '/box/delete?idbox=' + idbox;
    console.log(service);
    this.http.get(service)
      .subscribe (res => {
        let data = res.json();
        console.log(data);
        this.getCajas();
      }
      , err => {
        console.log(err);
      }
    );
  }

}
