import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DescargarMapaPage } from './descargar-mapa.page';

describe('DescargarMapaPage', () => {
  let component: DescargarMapaPage;
  let fixture: ComponentFixture<DescargarMapaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DescargarMapaPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DescargarMapaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
