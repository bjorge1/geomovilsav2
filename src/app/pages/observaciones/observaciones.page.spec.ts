import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ObservacionesPage } from './observaciones.page';

describe('ObservacionesPage', () => {
  let component: ObservacionesPage;
  let fixture: ComponentFixture<ObservacionesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ObservacionesPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ObservacionesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
