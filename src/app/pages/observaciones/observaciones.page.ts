import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { DataService } from "../../providers/index.services";

@Component({
  selector: 'app-observaciones',
  templateUrl: './observaciones.page.html',
  styleUrls: ['./observaciones.page.scss'],
})
export class ObservacionesPage implements OnInit {

  observaciones: any;

  habilitado: any = false;

  constructor(public navCtrl: NavController, private dataService: DataService ) {
    //this.observaciones = this.navParams.get('callback');
  }

  ngOnInit() {
    this.observaciones = [{
      ctitulo:  "",
      cobservacion: ""
    }];
  }




  add_observaciones(){

      var data ={
        ctitulo:  "",
        cobservacion: ""
      }
      this.observaciones.push(data);

    if(this.observaciones.length == 4){
      this.habilitado = true;
    }

  }
  goToPreviousPage(item:any){
    //console.log(item);
    this.navCtrl.navigateBack('/salida').then(() => {
      //console.log(item);
      this.dataService.changeData(item);

    });
  }

}
