import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { ToastController, ModalController } from '@ionic/angular';
import { Plugins, FilesystemDirectory, FilesystemEncoding } from '@capacitor/core';
import { File } from '@ionic-native/file/ngx';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { DescargarMapaPage } from '../descargar-mapa/descargar-mapa.page';
import { DescargarService } from 'src/app/providers/index.services';

const { Filesystem } = Plugins;

@Component({
  selector: 'app-descargar',
  templateUrl: './descargar.page.html',
  styleUrls: ['./descargar.page.scss'],
})


export class DescargarPage implements OnInit {
  public myimage = 'assets/gifs/loading.svg';
  hidden = this.descargarService.hidden;
  disabled = this.descargarService.disabled;
  fileTransfer: FileTransferObject;
  coordenadas: any;
  box: any;
  constructor(private nativeStorage: NativeStorage,
              public modalCtrl: ModalController,
              private descargarService: DescargarService) {
              }

  ngOnInit() {
  }

  addEvent() {
    if (this.myimage === ''){
      this.myimage = 'assets/gifs/loading.svg';
    } else {
      this.myimage = 'assets/gifs/loading.svg';
    }
  }

  async openModal() {
    const modal = await this.modalCtrl.create({
      component: DescargarMapaPage,
      cssClass: 'my-custom-modal-css'
    });
    return await modal.present();
  }

  cancelFileLoad() {
    this.myimage = '';
    this.hidden = true;
    this.disabled = false;
  }

  loadAcciones() {
    this.nativeStorage.getItem('center')
    .then(
      data => {
        this.coordenadas = data;
        this.box = this.coordenadas.box;
        const gjsonurl= 'http://179.43.81.84:8080/geoserver/uefsa/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=uefsa:sp_acciones&cql_filter=BBOX(ideasg,'
                        + this.box + ')&outputFormat=application%2Fjson';
        const message = 'Acciones descargadas correctamente.';
        const tipo = 'acciones';
        this.descargarService.loadGjson(gjsonurl, message, tipo);
        console.log(this.box);
      }
      , error => console.error('Error storing item', error)
    );
  }

  loadProyectos() {
    this.nativeStorage.getItem('center')
    .then(
      data => {
        this.coordenadas = data;
        this.box = this.coordenadas.box;
        const gjsonurl= 'http://179.43.81.84:8080/geoserver/uefsa/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=uefsa:sp_inversiones&cql_filter=BBOX(ideasg,'
                        + this.box + ')&outputFormat=application%2Fjson';
        const message = 'Inversiones descargadas correctamente.';
        const tipo = 'proyectos';
        this.descargarService.loadGjson(gjsonurl, message, tipo);
      }
      , error => console.error('Error getting item.', error)
    );
  }

  loadLimiteDepartamental() {
    this.nativeStorage.getItem('center')
    .then(
      data => {
        this.coordenadas = data;
        this.box = this.coordenadas.box;
        const gjsonurl= 'http://179.43.81.84:8080/geoserver/uefsa/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=uefsa:sp_departamentos&cql_filter=BBOX(ideasg,'
                        + this.box + ')&outputFormat=application%2Fjson';
        const message = 'Limites Departamentales descargadas correctamente.';
        const tipo = 'limdepart';
        this.descargarService.loadGjson(gjsonurl, message, tipo);
      }
      , error => console.error('Error getting item.', error)
    );
  }

  loadLimiteProvincial() {
    this.nativeStorage.getItem('center')
    .then(
      data => {
        this.coordenadas = data;
        this.box = this.coordenadas.box;
        const gjsonurl= 'http://179.43.81.84:8080/geoserver/uefsa/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=uefsa:sp_provincias&cql_filter=BBOX(ideasg,'
                        + this.box + ')&outputFormat=application%2Fjson';
        const message = 'Limites Provinciales descargadas correctamente.';
        const tipo = 'limprov';
        this.descargarService.loadGjson(gjsonurl, message, tipo);
      }
      , error => console.error('Error getting item.', error)
    );
  }

  loadLimiteDistrital() {
    this.nativeStorage.getItem('center')
    .then(
      data => {
        this.coordenadas = data;
        this.box = this.coordenadas.box;
        const gjsonurl= 'http://179.43.81.84:8080/geoserver/uefsa/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=uefsa:sp_distritos&cql_filter=BBOX(ideasg,'
                        + this.box + ')&outputFormat=application%2Fjson';
        const message = 'Limites Distritales descargadas correctamente.';
        const tipo = 'limdist';
        this.descargarService.loadGjson(gjsonurl, message, tipo);
      }
      , error => console.error('Error getting item.', error)
    );
  }

  loadCentrosPoblados() {
    this.nativeStorage.getItem('center')
    .then(
      data => {
        this.coordenadas = data;
        this.box = this.coordenadas.box;
        const gjsonurl= 'http://179.43.81.84:8080/geoserver/uefsa/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=uefsa:sp_centros_poblados&cql_filter=BBOX(ideasg,'
                        + this.box + ')&outputFormat=application%2Fjson';
        const message = 'Centros poblados descargados correctamente.';
        const tipo = 'centpobl';
        this.descargarService.loadGjson(gjsonurl, message, tipo);
      }
      , error => console.error('Error getting item.', error)
    );
  }

  loadRedVialNacional() {
    this.nativeStorage.getItem('center')
    .then(
      data => {
        this.coordenadas = data;
        this.box = this.coordenadas.box;
        const gjsonurl= 'http://179.43.81.84:8080/geoserver/uefsa/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=uefsa:sp_rv_nacional&cql_filter=BBOX(ideasg,'
                        + this.box + ')&outputFormat=application%2Fjson';
        const message = 'Red Vial Nacional descargada correctamente.';
        const tipo = 'limdepart';
        this.descargarService.loadGjson(gjsonurl, message, tipo);
      }
      , error => console.error('Error getting item.', error)
    );
  }

  loadRedVialDepartamental() {
    this.nativeStorage.getItem('center')
    .then(
      data => {
        this.coordenadas = data;
        this.box = this.coordenadas.box;
        const gjsonurl= 'http://179.43.81.84:8080/geoserver/uefsa/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=uefsa:sp_rv_nacional&cql_filter=BBOX(ideasg,'
                        + this.box + ')&outputFormat=application%2Fjson';
        const message = 'Red Vial Departamental descargada correctamente.';
        const tipo = 'rvdepart';
        this.descargarService.loadGjson(gjsonurl, message, tipo);
      }
      , error => console.error('Error getting item.', error)
    );
  }

  loadRedVialVecinal() {
    this.nativeStorage.getItem('center')
    .then(
      data => {
        this.coordenadas = data;
        this.box = this.coordenadas.box;
        const gjsonurl= 'http://179.43.81.84:8080/geoserver/uefsa/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=uefsa:sp_rv_vecinal&cql_filter=BBOX(ideasg,'
                        + this.box + ')&outputFormat=application%2Fjson';
        const message = 'Red Vial Vecinal descargada correctamente.';
        const tipo = 'rvnacional';
        this.descargarService.loadGjson(gjsonurl, message, tipo);
      }
      , error => console.error('Error getting item.', error)
    );
  }

}
