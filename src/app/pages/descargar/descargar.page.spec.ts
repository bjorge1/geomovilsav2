import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DescargarPage } from './descargar.page';

describe('DescargarPage', () => {
  let component: DescargarPage;
  let fixture: ComponentFixture<DescargarPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DescargarPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DescargarPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
