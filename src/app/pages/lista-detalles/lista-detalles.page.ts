import { Component, OnInit } from '@angular/core';
import { DataService, DatabaseService } from 'src/app/providers/index.services';
import { BehaviorSubject } from 'rxjs';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-lista-detalles',
  templateUrl: './lista-detalles.page.html',
  styleUrls: ['./lista-detalles.page.scss'],
})
export class ListaDetallesPage implements OnInit {
  idTblPunto: any;
  idTbl: any;
  fotos = new BehaviorSubject([]);
  fotosNuevas = new BehaviorSubject([]);
  constructor(private navCtrl: NavController, private dataService: DataService, private dataBaseService: DatabaseService) { }

  ngOnInit() {
    this.idTbl = this.dataService.idTabla;
    console.log(this.idTbl);
    this.dataBaseService.loadFotosById(this.idTbl);
    this.fotos = this.dataBaseService.fotosById;

    this.idTblPunto = this.dataService.idTablaNuevo;
    console.log(this.idTblPunto);
    this.dataBaseService.loadNuevasFotosById(this.idTblPunto);
    this.fotosNuevas = this.dataBaseService.nuevasFotosByid;
    console.log(this.fotos);
  }

  goBack() {
    this.navCtrl.pop();
  }

}
