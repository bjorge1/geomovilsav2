import { Component, OnInit,  } from '@angular/core';
import { NavController, ToastController, Platform, ActionSheetController } from '@ionic/angular';


import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { DataService } from "../../providers/index.services";



//import 'proj4';
import proj4 from 'proj4';
import { GeolocationOptions } from '@capacitor/core';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { LocationAccuracy } from '@ionic-native/location-accuracy/ngx';

//import proj4 from '../../../assets/proj4js-2.4.1/projs';
//import * as proj4 from 'proj4';

//import * as proj4 from 'proj4';


//import * as proj4 from 'proj4';
//import proj4 from 'proj4'




@Component({
  selector: 'app-foto',
  templateUrl: './foto.page.html',
  styleUrls: ['./foto.page.scss'],
})

export class FotoPage implements OnInit {

  hideMe:boolean = true;
  showPin:boolean = true;
  hideMe2 = false;
  photo2: string;
  habilitar: any;
  mostrar: any = false;



  show_longitud: any;
  show_latitud: any;
  imagepreview = '';
  fototemp: any;
  watchLocationUpdates:any;


  constructor(private navCtrl: NavController, private camera: Camera,
              private toastCtrl: ToastController, private platform: Platform,
              private geolocation: Geolocation, private dataService: DataService,
              public actionSheetController: ActionSheetController,
              private androidPermissions: AndroidPermissions,
              private locationAccuracy: LocationAccuracy ) {
    //console.log(proj4.defs);
    this.fototemp = {
      img: "",
      ctitulo: "",
      cobservacion: "",
      zone: "",
      longitud: "",
      latitud: ""
    }
  }

  ngOnInit() {
    this.habilitar = false;
    this.mostrar = false;
    console.log(this.dataService.origin);

    this.platform.ready().then(()=>{
      this.photo2 = 'assets/img/nature.png';

    });
  }

  ionViewWillEnter(){
    if(this.dataService.origin == 'fromNueva') {
      this.showPin == true;
    }else{
      this.showPin == false;
    }

  }
  goBack(){

    if(this.dataService.origin == 'fromNueva'){
      this.navCtrl.navigateBack('/fotos/fromNueva');
    }else{
      this.navCtrl.navigateBack('/fotos/fromSalida');
    }
  }

  takePicture(sourceType) {
    const cameraOptions: CameraOptions = {
      quality: 50,
      sourceType: sourceType,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true
    }
    this.mostrar = true;
    this.camera.getPicture(cameraOptions).then((imageData) => {
      // this.camera.DestinationType.FILE_URI gives file URI saved in local
      // this.camera.DestinationType.DATA_URL gives base64 URI
      this.imagepreview = 'data:image/jpeg;base64,' + imageData;
      this.fototemp.img = 'data:image/jpeg;base64,' + imageData;

    }, (err) => {
      console.log(err);
      // Handle error
    });
    if(this.dataService.origin == 'fromNueva'){
      this.showPin == true;
      console.log("is here")
    }else{
      this.showPin == false;
    }
    this.hideMe = false
    this.hideMe2 = true;
  }
  async selectImage() {
    const actionSheet = await this.actionSheetController.create({
      header: "Selecciona la fuente de la imagen: ",
      buttons: [{
        text: 'Cargar desde la galería',
        icon: 'images',
        handler: () => {
          this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
        }
      },
      {
        text: 'Tomar una foto',
        icon: 'camera',
        handler: () => {
          this.takePicture(this.camera.PictureSourceType.CAMERA);
        }
      },
      {
        text: 'Cancelar',
        role: 'cancel',
        icon: 'close'
      }
      ]
    });
    await actionSheet.present();
  }

  sendData() {
    var  objeto = {
      img: "",
      ctitulo: "",
      cobservacion: "",
      zone: '',
      longitud: '',
      latitud: ''
    }
    console.log(this.dataService.origin)
    if(this.dataService.origin == "fromNueva"){
      console.log("Envio de fromNueva")
      this.navCtrl.navigateBack('/fotos/fromNueva').then(() => {
        objeto.img = this.fototemp.img;
        objeto.ctitulo = this.fototemp.ctitulo;
        objeto.cobservacion = this.fototemp.cobservacion;
        console.log(objeto);
        this.dataService.changeDataFoto(objeto);
      });
    }else if(this.dataService.origin == "fromSalida"){
      console.log("Envio de fromSalida")
      this.navCtrl.navigateBack('/fotos/fromSalida').then(() => {
        objeto.img = this.fototemp.img;
        objeto.ctitulo = this.fototemp.ctitulo;
        objeto.cobservacion = this.fototemp.cobservacion;
        objeto.zone = this.fototemp.zone;
        objeto.longitud = this.fototemp.longitud;
        objeto.latitud = this.fototemp.latitud;
        console.log(objeto);
        this.dataService.changeDataFoto(objeto);

      });
    }

  }

  getGeolocation(){
    this.habilitar = true;
    this.geolocation.getCurrentPosition(
      {maximumAge: 1000, timeout: 20000,
       enableHighAccuracy: true }
      )
      .then((resp) => {
      this.show_latitud = resp.coords.latitude;
      this.show_longitud = resp.coords.longitude;
      var ZoneNumber = Math.floor((this.show_longitud  + 180)/6) + 1;
      this.fototemp.zone = ZoneNumber;
      this.fototemp.longitud = this.show_longitud;
      this.fototemp.latitud = this.show_latitud;
      console.log(ZoneNumber);
      console.log(this.show_latitud);
      console.log(this.show_longitud);
     }).catch((error) => {
       alert('Error getting location'+ JSON.stringify(error));
     });
  }

  // Check if application having GPS access permission
  checkGPSPermission() {
    this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION).then(
      result => {
        if (result.hasPermission) {

          // If having permission show 'Turn On GPS' dialogue
          this.askToTurnOnGPS();
        } else {

          // If not having permission ask for permission
          this.requestGPSPermission();
        }
      },
      err => {
        alert(err);
      }
    );
  }

  requestGPSPermission() {
    this.locationAccuracy.canRequest().then((canRequest: boolean) => {
      if (canRequest) {
        console.log("4");
      } else {
        // Show 'GPS Permission Request' dialogue
        this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION)
          .then(
            () => {
              // call method to turn on GPS
              this.askToTurnOnGPS();
            },
            error => {
              // Show alert if user click on 'No Thanks'
              alert('requestPermission Error requesting location permissions ' + error)
            }
          );
      }
    });
  }

  askToTurnOnGPS() {
    this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
      () => {
        // When GPS Turned ON call method to get Accurate location coordinates
        this.getGeolocation();
      },
      error => alert('Error requesting location permissions ' + JSON.stringify(error))
    );
  }

  getPosicion(){
    this.habilitar = true;
    var options = { maximumAge: 3000, timeout: 20000, enableHighAccuracy: true };
    var watchId  = navigator.geolocation.watchPosition(onSuccess, onError, options);
    console.log(watchId);

    function onSuccess (position:any){
      this.show_longitud = position.coords.longitude;
      this.show_latitud = position.coords.latitude;
      var longitude = position.coords.longitude;
      var latitude = position.coords.latitude;
      console.log("longitude-->"+ longitude + " latitude-->"+ latitude);
      this.show_longitud = this.longitude;
      this.show_latitud = this.latitude;


      // agregando la conversionde de coordenadas wgs84 a utm

      var ZoneNumber = Math.floor((longitude + 180)/6) + 1;
      console.log(ZoneNumber);
      // proj4.defs('WGS84', "+title=WGS 84 (long/lat) +proj=longlat +ellps=WGS84 +datum=WGS84 +units=degrees");

      proj4.defs([
                   [
                     'EPSG:4326',
                     '+title=WGS 84 (long/lat) +proj=longlat +ellps=WGS84 +datum=WGS84 +units=degrees'],
                   [
                     'EPSG:32718',
                     "+proj=utm +zone=18 +south +ellps=WGS84 +datum=WGS84 +units=m +no_defs"
                   ],
                   [
                     'EPSG:32717',
                     "+proj=utm +zone=17 +south +ellps=WGS84 +datum=WGS84 +units=m +no_defs"
                   ],
                   [
                     'EPSG:32719',
                     "+proj=utm +zone=19 +south +ellps=WGS84 +datum=WGS84 +units=m +no_defs"
                   ]
                 ]);
      var coordenadas = proj4('EPSG:4326', 'EPSG:327'+ZoneNumber,[longitude,latitude]);
      var est=parseInt(coordenadas[0]);
      var nort=parseInt(coordenadas[1]);

      console.log(est);
      console.log(nort);


      }

    function onError(error:any){
       console.log("the code is " + error.code + " " + "message: " + error.message);
      }

      /*this.fototemp.longitud = temp_longitud;
      this.fototemp.latitud = temp_latitud;
      this.fototemp.zone = temp_zone;*/
  }

  obtenerPosicion() {
    console.log('cargando...');
    var onSuccess = function(position) {
      alert('Latitude: '          + position.coords.latitude          + '\n' +
            'Longitude: '         + position.coords.longitude         + '\n' +
            'Altitude: '          + position.coords.altitude          + '\n' +
            'Accuracy: '          + position.coords.accuracy          + '\n' +
            'Altitude Accuracy: ' + position.coords.altitudeAccuracy  + '\n' +
            'Heading: '           + position.coords.heading           + '\n' +
            'Speed: '             + position.coords.speed             + '\n' +
            'Timestamp: '         + position.timestamp                + '\n');
  };

  // onError Callback receives a PositionError object
  //
    function onError(error) {
      alert('code: '    + error.code    + '\n' +
            'message: ' + error.message + '\n');
  }

    navigator.geolocation.getCurrentPosition(onSuccess, onError);
  }

  async presentToast(msg_lon:any, msg_lat:any) {
    const toast = await this.toastCtrl.create({
      message: 'Coordenadas -->'+msg_lon +' - '+msg_lat,
      duration: 10000
    });
    toast.present();
  }
}
