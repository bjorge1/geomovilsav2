import { Component, OnInit } from '@angular/core';
import { NavController, Platform, ToastController } from '@ionic/angular';
import { ActivatedRoute} from '@angular/router';

import { DataService } from '../../providers/index.services';

// Const ITEMS_KEY = 'my-items';
@Component({
  selector: 'app-fotos',
  templateUrl: './fotos.page.html',
  styleUrls: ['./fotos.page.scss'],
})

export class FotosPage implements OnInit {


  habilitar: boolean = false;
  imagen1: any = "";
  ocultar0: boolean = true;
  ocultar1: boolean = false;
  origin: any;
  constructor(private navCtrl: NavController,
              private toastController: ToastController,
              public platform: Platform,
              public dataService: DataService,
              private activatedRoute: ActivatedRoute ) {

  }

  ngOnInit() {

    this.origin = this.activatedRoute.snapshot.paramMap.get('origin');
    this.dataService.origin = this.origin;
    console.log("Corresponde a --> "+this.origin);
    this.platform.ready().then(() => {
      this.dataService.serviceDataFoto
        .subscribe( data => (this.dataService.colecciones = data ));
        console.log( "sent data from observaciones page : ", this.dataService.colecciones );

    });

  /*  this.colecciones = [{
      img: "assets/img/nature.png",
      ctitulo: "aaaaaaaaaaaa",
      cobservacion: "bbbbbbbbbbbbbbbbbbbbb"
    },
    {
      img: "assets/img/nature.png",
      ctitulo: "cccccccccccc",
      cobservacion: "ddddddddddddddddddddd"
    }]*/

  }
  ionViewWillEnter(){

    console.log(this.dataService.colecciones.length);
    if(this.dataService.colecciones.length == 2){
      this.habilitar = true;
    }else{
      this.habilitar = false;
    }
    if(this.dataService.colecciones.length > 0){
      this.ocultar0 = false;
      this.ocultar1 = true;
    }else{
      this.ocultar0 = true;
      this.ocultar1 = false;
    }

  }
  goToPreviousPage(item:any){

    if( this.dataService.origin == 'fromNueva'){
      this.navCtrl.navigateBack('/nueva-qocha').then(() => {
        console.log(item);
        this.dataService.changeDataColeccionesv2(item);
        

      });
    } else if( this.dataService.origin == 'fromSalida'){
      this.navCtrl.navigateBack('/salida/' + this.dataService.id_salida).then(() => {
        console.log("salida");
        this.dataService.changeDataColecciones(item);

      });
    }

  }
  coleccion() {
    this.navCtrl.navigateForward('/foto');
  }

  deleteItem(item: any) {

    for( var i = 0; i < this.dataService.colecciones.length; i++){

       if ( this.dataService.colecciones[i].ctitulo === item.ctitulo) {
         this.dataService.colecciones.splice(i, 1);
       }
    }
    if(this.dataService.colecciones.length > 0){
      this.ocultar0 = false;
      this.ocultar1 = true;
    }else{
      this.ocultar0 = true;
      this.ocultar1 = false;
    }
    if(this.dataService.colecciones.length == 2){
      this.habilitar = true;
    }else{
      this.habilitar = false;
    }
    console.log(this.dataService.colecciones);

  }

  goBack() {
    if(this.origin == "fromNueva"){
      this.navCtrl.navigateBack('/nueva-qocha');
    }else{
      this.navCtrl.navigateBack('/salida/' + this.dataService.id_salida);
    }

  }

  async showToast(msg: any) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }

}
