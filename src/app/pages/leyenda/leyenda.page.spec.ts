import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeyendaPage } from './leyenda.page';

describe('LeyendaPage', () => {
  let component: LeyendaPage;
  let fixture: ComponentFixture<LeyendaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeyendaPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeyendaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
