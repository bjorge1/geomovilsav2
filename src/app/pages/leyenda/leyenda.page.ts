import { Component, OnInit } from '@angular/core';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';

@Component({
  selector: 'app-leyenda',
  templateUrl: './leyenda.page.html',
  styleUrls: ['./leyenda.page.scss'],
})
export class LeyendaPage implements OnInit {

  constructor(
    private photoViewer: PhotoViewer
  ) {
    
  }

  ngOnInit() {
    // var url='http://190.117.83.13:8081/geoserver16/uefsa/wms?REQUEST=GetLegendGraphic&VERSION=1.0.0&FORMAT=image/png&WIDTH=20&HEIGHT=20&LAYER=uefsa:offline';
    // this.photoViewer.show(url);
  }

}
