import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { FormBuilder, FormGroup } from "@angular/forms";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import {
  NavController,
  ModalController,
  LoadingController
} from "@ionic/angular";
import { DataService, DatabaseService } from "../../providers/index.services";
import { NativeStorage } from "@ionic-native/native-storage/ngx";
@Component({
  selector: "app-nueva-qocha",
  templateUrl: "./nueva-qocha.page.html",
  styleUrls: ["./nueva-qocha.page.scss"]
})
export class NuevaQochaPage implements OnInit {
  form: FormGroup;

  infraestructuras = [
    {
      id: "siembra",
      name: "Siembra"
    },
    {
      id: "cosecha",
      name: "Cosecha"
    },
    {
      id: "otros",
      name: "Otros"
    }
  ];

  siembras = [
    {
      id: "qochas",
      name: "Qochas"
    },
    {
      id: "amunas",
      name: "Amunas"
    },
    {
      id: "zanjas",
      name: "Zanjas"
    },
    {
      id: "reforestacion para servicio hidrico",
      name: "Reforestación para servicio hidrico"
    },
    {
      id: "praderas altoandinas",
      name: "Praderas altoandinas"
    },
    {
      id: "bofedales",
      name: "Bofedales"
    },
    {
      id: "otros",
      name: "Otros"
    }
  ];
  cosechas = [
    {
      id: "microreservorios",
      name: "Microreservorios"
    },
    {
      id: "canales de riego",
      name: "Canales de Riego"
    },
    {
      id: "otros",
      name: "Otros"
    }
  ];

  siembrahabilitar: boolean = false;
  cosechahabilitar: boolean = false;
  otroshabilitar: boolean = false;

  items: any;
  fotos: any;
  data: any = {
    infraestructura: "",
    subtipo: "",
    longitud: "",
    latitud: ""
  };
  count_fotos: any;
  habilitarotros: boolean = false;

  tempotros: any;
  tempinfraestructura: any;
  idusuario: any;
  addInfraestructura: any;
  addDetalle: any;
  constructor(
    fb: FormBuilder,
    private _cdr: ChangeDetectorRef,
    public loadingController: LoadingController,
    private nativeStorage: NativeStorage,
    private navCtrl: NavController,
    private modalCtrl: ModalController,
    public dataService: DataService,
    private databaseService: DatabaseService
  ) {}

  ngOnInit() {
    this.dataService.serviceDataColecionv2.subscribe(
      data => (this.fotos = data)
    );
    console.log("Datos obtenidos: ", this.fotos);
    console.log(this.addDetalle);
  }
  ionViewWillEnter() {
    /*console.log(this.observations);
    this.count_observaciones = this.observations.length;*/
    console.log(this.fotos);
    this.count_fotos = this.fotos.length;
    console.log("Longitud: " + this.dataService.getLng());
  }

  onChangeInfraestructura(): void {
    console.log(this.data.infraestructura);
    if (this.data.infraestructura == "siembra") {
      this.siembrahabilitar = true;
      this.cosechahabilitar = false;
      this.otroshabilitar = false;
    } else if (this.data.infraestructura == "cosecha") {
      this.cosechahabilitar = true;
      this.siembrahabilitar = false;
      this.otroshabilitar = false;
    } else {
      this.otroshabilitar = true;
      this.cosechahabilitar = false;
      this.siembrahabilitar = false;
    }
  }

  onChangeSiembra(): void {
    if (this.data.subtipo == "otros") {
      this.habilitarotros = true;
    } else {
      this.habilitarotros = false;
    }
  }

  onChangeCosecha(): void {
    if (this.data.subtipo == "otros") {
      this.habilitarotros = true;
    } else {
      this.habilitarotros = false;
    }
  }

  takingPhotos() {
    this.navCtrl.navigateRoot("/fotos/fromNueva");
  }

  salida(id) {
    id = this.form.get("country").value;
    this.navCtrl.navigateRoot("/salida/" + id);
  }

  async closeModal() {
    await this.modalCtrl.dismiss();
  }

  saveNuevo() {
    this.presentLoadingWithOptions("Guardando...");

    /*this.nativeStorage.getItem('usuario')
    .then(
      async data => {

        this.items = data;
        this.idusuario = this.items.idUsuario;

      },
      error => console.error(error)
    );*/

    this.idusuario = this.dataService.id_usuario;
    console.log("id usuario --> " + this.idusuario);

    if (this.data.infraestructura == "otros") {
      this.data.infraestructura = this.tempinfraestructura;
      this.data.subtipo = this.tempotros;
    }

    if (this.data.subtipo == "otros") {
      this.data.subtipo = this.tempotros;
    }

    console.log(this.data);

    this.data.longitud = this.dataService.getLng();
    this.data.latitud = this.dataService.getLat();
    console.log(this.data.longitud);
    console.log(this.data.latitud);

    this.databaseService
      .addNuevo(this.data, this.idusuario, this.fotos)
      .then(() => {
        this.navCtrl.navigateRoot("/menu");
        for (var i = 0; i < this.dataService.colecciones.length; i++) {
          this.dataService.colecciones.splice(
            i,
            this.dataService.colecciones.length
          );
        }
        this.count_fotos = 0;
        this.dataService.colecciones = [];
        this.dataService.temp3 = [];
        this.dataService.temp = [];
      });
  }

  async presentLoadingWithOptions(mensaje: any) {
    const loading = await this.loadingController.create({
      spinner: "bubbles",
      duration: 3000,
      message: mensaje,
      translucent: true,
      cssClass: "custom-class custom-loading"
    });

    return await loading.present();
  }

  volver() {
    this.navCtrl.navigateBack("/menu/main");
  }
}
