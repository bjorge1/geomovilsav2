import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-ubicacion',
  templateUrl: './ubicacion.page.html',
  styleUrls: ['./ubicacion.page.scss'],
})
export class UbicacionPage implements OnInit {

  constructor(public navCtrl: NavController) {

  }

  ngOnInit() {
  }

  goBack(){
    this.navCtrl.navigateBack('/salida').then(() => {

      //console.log(item);
      //this.dataService.changeData(item);



    });
  }

}
