import { Component } from "@angular/core";
import { File } from "@ionic-native/file/ngx";
import {
  FileTransfer,
  FileTransferObject
} from "@ionic-native/file-transfer/ngx";
import { NavController, Platform, ToastController } from "@ionic/angular";
import { FilePath } from "@ionic-native/file-path/ngx";
import { DataService } from 'src/app/providers/index.services';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
@Component({
  selector: "app-selector",
  templateUrl: "./selector.page.html",
  styleUrls: ["./selector.page.scss"]
})
export class SelectorPage {
  storageDirectory: string = "";
  files: any;
  selectedMapa = '';

  constructor(
    public navCtrl: NavController,
    private filePath: FilePath,
    public platform: Platform,
    private transfer: FileTransfer,
    private file: File,
    private dataservice: DataService,
    private toastCtrl: ToastController,
    private nativeStorage: NativeStorage
  ) {
    this.platform.ready().then(() => {
      file.listDir(file.externalRootDirectory, "UEFSA/Mbtiles").then(result => {
        console.log(result);
        this.files = result;
        console.log(this.files);
        /*result will have an array of file objects with 
        file details or if its a directory*/
        for (let file of result) {
          if (
            file.isDirectory === true &&
            file.name !== "." &&
            file.name !== ".."
          ) {
            // Code if its a folder
          } else if (file.isFile === true) {
            // Code if its a file
            let name = file.name; // File name
            let path = file.nativeURL; // File path
            console.log(path);
            file.getMetadata(function(metadata) {
              let size = metadata.size; // Get file size
            });
          }
        }
      });
    });
  }

  setMapa(directory) {
    console.log(directory);
    this.dataservice.setMapa(directory.nativeURL);
    var regExp = /\(([^)]+)\)/;
    var matches = regExp.exec(directory.name);
    var x = matches[1].split(',')[0];
    var y = matches[1].split(',')[1];
    this.nativeStorage.setItem('center', {
      ejeX: x,
      ejeY: y,
    })
    .then(data => {
      console.log(data);
    }, error => console.error('Error storing item', error)
    );
    this.presentToast(directory.name.split('(')[0]);
  }

  async presentToast(title) {
    const toast = await this.toastCtrl.create({
      message: 'Mapa Seleccionado : ' + title,
      duration: 10000
    });
    toast.present();
  }
}
