import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { SQLitePorter } from '@ionic-native/sqlite-porter/ngx';
import { SQLite } from '@ionic-native/sqlite/ngx';
import { HttpClientModule } from '@angular/common/http';

import { DatePicker } from '@ionic-native/date-picker/ngx';
import { DatePipe } from '@angular/common';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { Camera } from '@ionic-native/camera/ngx';

/*import { Proj } from  'proj4';*/

import { FileTransfer } from '@ionic-native/file-transfer/ngx';
import { File } from '@ionic-native/file/ngx';
import { HttpModule } from '@angular/http';
import { FilePath } from '@ionic-native/file-path/ngx';
import { FileChooser } from '@ionic-native/file-chooser/ngx';
import { AES256 } from '@ionic-native/aes-256/ngx';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { LocationAccuracy } from '@ionic-native/location-accuracy/ngx';
import { DescargarMapaPageModule } from './pages/descargar-mapa/descargar-mapa.module';
import { NuevaQochaPageModule } from './pages/nueva-qocha/nueva-qocha.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';
import { LeyendaPageModule } from './pages/leyenda/leyenda.module';

@NgModule({
  declarations: [
  AppComponent

  ],
  entryComponents: [
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    HttpModule,
    FormsModule,
    DescargarMapaPageModule,
    NuevaQochaPageModule,
    LeyendaPageModule
    ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy},
    SQLite,
    SQLitePorter,
    DatePicker,
    DatePipe,
    NativeStorage,
    Camera,
    File,
    FileTransfer,
    FileChooser,
    FilePath,
    AES256,
    AndroidPermissions,
    Geolocation,
    LocationAccuracy,
    PhotoViewer
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
