import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { HttpClient } from '@angular/common/http';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite/ngx';

export interface Item {
  id: number;
  photo: {};
  title: string;
  description: string;
}

const DATA_BASE_NAME = 'fotos.db';

@Injectable({
  providedIn: 'root'
})
export class FotosService {

  database: SQLiteObject;
  ready: Promise<void>;

  constructor(public http: HttpClient, public sqlite: SQLite, private platform: Platform) { }
  initializeDatabase() {
    return this.sqlite.create({
        name: DATA_BASE_NAME,
        location: 'default'
    }).then((db: SQLiteObject) => {
       let sql = 'CREATE TABLE IF NOT EXISTS fotos(titulo TEXT PRIMARY KEY, photo TEXT, description TEXT)';
       return db.executeSql(sql, []);
    })
}

getDatabase() {
    return this.ready.then(() => {
        return this.sqlite.create({
            name: DATA_BASE_NAME,
            location: 'default'
        }).then((db: SQLiteObject) => {
            return db;
        })
    });
}

createFoto(titulo, photo, description) {
    console.log(titulo, photo, description);
    // let sql = 'INSERT INTO credentials(NULL,email,password) VALUES (?,?,?)';
    return this.sqlite.create({
        name: DATA_BASE_NAME,
        location: 'default'
    }).then((db: SQLiteObject) => {
     let sql = 'INSERT INTO fotos VALUES (NULL,?,?)';
     return db.executeSql(sql,[titulo, photo, description]);
    })
}

updateFoto(titulo, photo, description){
    return this.sqlite.create({
        name: DATA_BASE_NAME,
        location: 'default'
    }).then((db: SQLiteObject) => {
      let sql = 'UPDATE fotos SET description = ?, photo = ? WHERE titulo = ?';
      return db.executeSql(sql,[titulo, photo, description]);
    })
}

deleteFoto(titulo){
   return this.sqlite.create({
        name: DATA_BASE_NAME,
        location: 'default'
    }).then((db: SQLiteObject) => { 
      let sql = 'DELETE FROM fotos WHERE titulo = ?';
      return db.executeSql(sql,[titulo]);
    })
}
}
