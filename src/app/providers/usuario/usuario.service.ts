import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';


import { AlertController } from '@ionic/angular';

import { URL_SERVICES } from "../../config/url.services";

@Injectable({
  providedIn: 'root'
})

export class UsuarioService {
  token:any;
  tipo_usuario:string;
  id_user:any;
  ok: any;
  activeSearch = false;
  mode: any;
  constructor(public http: Http, private alertCtrl: AlertController) {
  }

  autenticacion( user:string, password:string){

    let url = URL_SERVICES + "/log/login?userId=" + user + "&password=" + password;

    return this.http.get( url )
      .map( res => {
        let data = res.json();
        console.log(data);
        if( data.data == null ){
            this.ok = false;
            this.presentAlert('Verifique sus credenciales!!!', '', 'Advertencia');
        } else {
          this.ok = true;
          this.id_user = data.data.idusuario;
          this.tipo_usuario = data.data.cnombrol;
          console.log("Id usuario: "+ this.id_user);
          console.log("Tipo usuario: " +this.tipo_usuario);
        }
      });
  }

  modoLogin(modo) {
    this.mode = modo;
  }

  modo() {
    return this.mode;
  }

  async presentAlert(mensaje: any, encabezado: any, sub: any ) {
    const alert = await this.alertCtrl.create({
      header: encabezado,
      subHeader: sub,
      message: mensaje,
      buttons: ['OK']
    });

    await alert.present();
  }

  getBoxes(id_user){
    let url = URL_SERVICES + "/box/select?idusuario=" + id_user;
    return this.http.get( url )
      .map( res => {
        let data = res.json();
        console.log(data);
      });
  }
}
