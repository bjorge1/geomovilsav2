import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { NativeStorage } from '@ionic-native/native-storage/ngx';

@Injectable({
  providedIn: 'root'
})
export class DescargarService {
  json: any;
  disabled = false;
  hidden = true;

  constructor(private toastCtrl: ToastController, private http: HttpClient, private nativeStorage: NativeStorage) { }

  async toast(mensaje) {
    const toast = await this.toastCtrl.create({
      message: mensaje,
      duration: 2000
    });
    toast.present();
  }

  loadGjson(gjsonurl, message, tipo) {
    this.disabled = true;
    this.hidden = false;
    const url = 'https://cors-anywhere.herokuapp.com/' + gjsonurl;
    this.http.get(url)
      .subscribe((json: any) => {
        console.log(json);
        this.json = json;
        this.nativeStorage.setItem(tipo, this.json)
          .then(
          async (data) => {
            this.toast(message);
            this.disabled = false;
            this.hidden = true;
            console.log(data);
          }, error => console.error('Error storing item', error)
          );
      });
  }
}
