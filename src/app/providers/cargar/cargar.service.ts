import { Injectable } from "@angular/core";
import { ToastController } from "@ionic/angular";
import { URL_SERVICES, URL_SERVICES_MOBILE } from "../../config/url.services";
import { Http } from "@angular/http";

@Injectable({
  providedIn: "root"
})
export class CargarService {
  constructor(public toastController: ToastController, private http: Http) {}

  sincronizarSalida(salida: any, foto: any) {
    let promesa = new Promise((resolve, reject) => {
      let url = URL_SERVICES_MOBILE + "/mobile/insert/salida";

      this.http.post(url, { salida }).subscribe(res => {
        let response = res.json();
        console.log(response);
        if (response.Error) {
          this.presentToast("Error!!!...");
          reject();
        } else {
          this.presentToast("Sincronizando...");
          this.sincronizarFoto(foto);
          resolve();
        }
      });
    });
    return promesa;
  }

  sincronizarFoto(foto: any) {
    let promesa = new Promise((resolve, reject) => {
      let url = URL_SERVICES_MOBILE + "/mobile/insert/foto";
      this.http.post(url, { foto }).subscribe(res => {
        let response = res.json();
        console.log(response);
        if (response.Error) {
          this.presentToast("Error!!!...");
          reject();
        } else {
          this.presentToast("Sincronizado...");
          resolve();
        }
      });
    });
    return promesa;
  }

  sincronizarFotoonly(fotoOnly: any) {
    let promesa = new Promise((resolve, reject) => {
      let url = URL_SERVICES_MOBILE + "/mobile/update/fotoOnly";

      this.http.post(url, { fotoOnly }).subscribe(res => {
        let response = res.json();
        console.log(response);
        if (response.Error) {
          this.presentToast("Error!!!...");
          reject();
        } else {
          this.presentToast("Sincronizado...");
          resolve();
        }
      });
    });
    return promesa;
  }

  sincronizarPunto(punto: any, foto: any) {
    let promesa = new Promise((resolve, reject) => {
      let url = URL_SERVICES_MOBILE + "/mobile/insert/punto";

      this.http.post(url, { punto }).subscribe(res => {
        let response = res.json();
        console.log(response);
        if (response.Error) {
          this.presentToast("Error!!!...");
          reject();
        } else {
          this.presentToast("Sincronizando...");
          this.sincronizarPuntoFoto(foto);
          resolve();
        }
      });
    });
    return promesa;
  }

  sincronizarPuntoFoto(foto: any) {
    let promesa = new Promise((resolve, reject) => {
      let url = URL_SERVICES_MOBILE + "/mobile/insert/fotopunto";

      this.http.post(url, { foto }).subscribe(res => {
        let response = res.json();
        console.log(response);
        if (response.Error) {
          this.presentToast("Error!!!...");
          reject();
        } else {
          //this.presentToast('Sincronizado...');
          resolve();
        }
      });
    });
    return promesa;
  }

  sincronizarPuntoFotoonly(fotoOnly: any) {
    let promesa = new Promise((resolve, reject) => {
      let url = URL_SERVICES_MOBILE + "/mobile/update/fotopuntoOnly";

      this.http.post(url, { fotoOnly }).subscribe(res => {
        let response = res.json();
        console.log(response);
        if (response.Error) {
          this.presentToast("Error!!!...");
          reject();
        } else {
          this.presentToast("Sincronizado...");
          resolve();
        }
      });
    });
    return promesa;
  }

  async presentToast(mensaje: any) {
    const toast = await this.toastController.create({
      message: mensaje,
      duration: 2000
    });
    toast.present();
  }
}
