import { Injectable } from "@angular/core";

import { BehaviorSubject } from "rxjs";

import { NativeStorage } from "@ionic-native/native-storage/ngx";

@Injectable({
  providedIn: "root"
})
export class DataService {
  [x: string]: any;

  colecciones: any = [];
  temp: any = [];
  temp1: any = [];
  temp2: any = [];
  temp3: any = [];
  idQocha: any;
  idTabla: any;
  idTablaNuevo: any;
  id_salida: any;
  id_usuario: any;
  origin: any;
  directory: any;

  latitud: any;
  longitud: any;

  private dataSource = new BehaviorSubject(this.temp1);
  private dataSourceFoto = new BehaviorSubject(this.temp);
  private dataSourceColeccion = new BehaviorSubject(this.temp2);
  private dataSourceColeccionv2 = new BehaviorSubject(this.temp3);
  serviceData = this.dataSource.asObservable();
  serviceDataFoto = this.dataSourceFoto.asObservable();
  serviceDataColecion = this.dataSourceColeccion.asObservable();
  serviceDataColecionv2 = this.dataSourceColeccionv2.asObservable();

  constructor(private nativeStorage: NativeStorage) {
    //this.changeData(this.item);
  }

  changeDataFoto(data: any) {
    //console.log(data);

    if (this.temp.length == 0) {
      //this.temp.push(data)
      this.temp.push(data);
      //console.log("cero")
      //console.log(this.temp);
      this.dataSourceFoto.next(this.temp);
    } else if (this.temp.length > 0) {
      this.temp.push(data);
      //console.log("uno")
      //console.log(this.temp);

      this.dataSourceFoto.next(this.temp);
    }
  }

  changeData(data: any) {
    //console.log(data);
    this.dataSource.next(data);
    //this.temp1 = data;
  }

  changeDataColecciones(data: any) {
    //console.log(data);
    this.dataSourceColeccion.next(data);
    //this.temp1 = data;
  }
  changeDataColeccionesv2(data: any) {
    //console.log(data);
    this.dataSourceColeccionv2.next(data);
    //this.temp1 = data;
  }

  setLatLong(lat, long) {
    this.latitud = lat;
    this.longitud = long;
  }

  getLat() {
    return this.latitud;
  }
  getLng() {
    return this.longitud;
  }

  setIdQocha(id) {
    this.idQocha = id;
  }

  setIdTbl(idTbl) {
    this.idTabla = idTbl;
  }

  setIdTblPntNuevo(idTblNuevo) {
    this.idTablaNuevo = idTblNuevo;
  }

  setMapa(dir) {
    this.directory = dir;
    this.nativeStorage.setItem("srcMap", {src: this.directory}).then(
      () => console.log("Stored item!"),
      error => console.error("Error storing item", error)
    );
  }

  setIdusuario(id: any) {
    this.nativeStorage
      .setItem("person", {
        id: id
      })
      .then(
        () => console.log("Stored item!"),
        error => console.error("Error storing item", error)
      );
  }
  getIdusuario() {
    this.nativeStorage.getItem("person").then(
      data => {
        console.log(data.id);
        this.id_usuario = data.id;
      },
      error => console.error(error)
    );
  }
}
