import { Injectable } from '@angular/core';
import { Platform, ToastController } from '@ionic/angular';
import { SQLitePorter } from '@ionic-native/sqlite-porter/ngx';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite/ngx';
import { BehaviorSubject, Observable } from 'rxjs';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { File } from '@ionic-native/file/ngx';

export interface Users {
  id_usuario: string,
  usuario: string,
  contrasena: string,
  id_personal: string,
  tipo_usuario: string,
  estado: boolean
}
/*
export interface Dept{
  ccoddpto: string,
  nombdpto: string
};

export interface Etap{
  idetapa: string,
  cnombetapa: string
};

export interface Process{
  idproceso: string,
  cnombproceso: string,
  idetapa: string
};*/

//salida - foto
export interface Salid{
  id_tblsalida: string,
  codigo: string,
  idusuario: string,
  fecha_registro: string,
  tipo: string
}
export interface Fot{
  id: string,
  id_tblsalida: string,
  img: String,
  ctitulo: string,
  cobservacion: string,
  zone: string,
  longitud: string,
  latitud: string

}
export interface Nuev{
  id_tblnuevo: string,
  idusuario: string,
  infraestructura: string,
  subtipo: string,
  fecha_registro: string,
  longitud: string,
  latitud: string
}

export interface Fotnuev{
  id: string,
  id_tblnuevo: string,
  img: String,
  ctitulo: string,
  cobservacion: string

}


@Injectable({
  providedIn: 'root'
})
export class DatabaseService {
  private database: SQLiteObject;
  private dbReady: BehaviorSubject<boolean> = new BehaviorSubject(false);
  fileTransfer: FileTransferObject;
  usuarios = new BehaviorSubject([]);
  // departamentos = new BehaviorSubject([]);
  // etapas = new BehaviorSubject([]);
  // procesos = new BehaviorSubject([]);
  salidas = new BehaviorSubject([]);
  fotos = new BehaviorSubject([]);
  fotosById = new BehaviorSubject([]);
  onlyFotos: any;

  nuevas = new BehaviorSubject([]);
  nuevasFotos = new BehaviorSubject([]);
  nuevasFotosByid = new BehaviorSubject([]);
  onlyNuevosFotos: any;

  generate: any;
  private _DB: SQLiteObject;
  private _DB_NAME: string = "sierra.db";

  constructor(private platform: Platform,
              private sqlitePorter: SQLitePorter,
              private sqlite: SQLite,
              private transfer: FileTransfer,
              private file: File,
              private toastCtrl: ToastController,
              private http: HttpClient) {
    this.platform.ready().then(() => {
      this.sqlite.create({
        name: this._DB_NAME,
        location: 'default'
      })
      .then((db: SQLiteObject) => {
          this.database = db;
          this.seedDatabase();
      });
    });
  }

  seedDatabase() {
    this.http.get('assets/bd/seed.sql', { responseType: 'text'})
    .subscribe(sql => {
      this.sqlitePorter.importSqlToDb(this.database, sql)
        .then(_ => {

          this.dbReady.next(true);
        })
        .catch(e => console.error(e));
    });
  }

  getDatabaseState() {
    return this.dbReady.asObservable();
  }

  public exportDbToSql() {
    this.sqlitePorter.exportDbToSql(this.database)
      .then((data) => this.exportSQLToFile(data))
      .catch(() => console.log('error'));
  }

  exportSQLToFile(data) {
    console.log(data);
    let path = '';
    let dir_name = 'UEFSA/Export';
    let file_name = 'data.sql';
    this.file.createDir(this.file.externalRootDirectory, 'UEFSA', true);
    let result = this.file.createDir(this.file.externalRootDirectory, dir_name, true);
    result.then((resp) => {
      path = resp.toURL();
      this.file.writeFile(path, file_name, data)
      .then(
        async (entry) => {
        const toast = await this.toastCtrl.create({
          message: 'Base de datos respaldada correctamente.',
          duration: 2000
          });
        toast.present();
        console.log('Respaldo completo: ' + entry.toURL());
      }, async (error: HttpErrorResponse) => {
        this.file.writeExistingFile(path, file_name, data);
        const toast = await this.toastCtrl.create({
          message: 'Se actualizó el archivo de respaldo.',
          duration: 2000
          });
        toast.present();
        console.log(error);
      });
    }, (err) => {
      console.log('Error creando entrada : ' + err);
    });

    let dir_name_import = 'UEFSA/Import';
    let result2 = this.file.createDir(this.file.externalRootDirectory, dir_name_import, true);
    result2.then(
      (resp) => {
      console.log('Carpeta import creada');
    }, (err) => {
      console.log('Error creando entrada : ' + err);
    });
  }

  importFileToSQL() {
    let win: any = window;
    let safeURL = win.Ionic.WebView.convertFileSrc('file:///storage/emulated/0/UEFSA/Import/data.sql');
    // this.file.resolveLocalFilesystemUrl(path)
    console.log(safeURL);
    this.http.get(safeURL, { responseType: 'text'})
    .subscribe(sql => {
      this.sqlitePorter.importSqlToDb(this.database, sql)
        .then(async _ => {
          this.dbReady.next(true);
          const toast = await this.toastCtrl.create({
            message: 'Base de datos importada correctamente.',
            duration: 2000
            });
          toast.present();
          console.log('Respaldo completo: ' + _);
        })
        .catch(async (e: HttpErrorResponse) => {
          const toast = await this.toastCtrl.create({
            message: 'No existe el archivo a importar.',
            duration: 2000
            });
          toast.present();
          console.error(e);
        });
    });
  }


  //Salida
  getSalidas(){
    // this.loadSalidas();
    return this.salidas;
  }
  async loadSalidas() {

       // code goes here
       console.log("Preparando para cargar...tblsalida")
       const data = await this.database.executeSql('SELECT * FROM tblsalida', []);
       let salidas = [];
       if (data.rows.length > 0) {
         for (var i = 0; i < data.rows.length; i++) {
           /*let skills = [];
           if (data.rows.item(i).skills != '') {
             skills = JSON.parse(data.rows.item(i).skills);
           }*/
           salidas.push({
             id_tblsalida: data.rows.item(i).id_tblsalida,
             codigo: data.rows.item(i).codigo,
             idusuario: data.rows.item(i).idusuario,
             fecha_registro: data.rows.item(i).fecha_registro,
             tipo: data.rows.item(i).tipo

           });
         }
       }
       this.salidas.next(salidas);


  }

  getFotos() {
    return this.fotos;
  }

 async loadFotos() {

     // code goes here

      const data = await this.database.executeSql('SELECT * FROM tblfoto', []);
      let fotos = [];
      if (data.rows.length > 0) {
        for (var i = 0; i < data.rows.length; i++) {
          /*let skills = [];
          if (data.rows.item(i).skills != '') {
            skills = JSON.parse(data.rows.item(i).skills);
          }*/
          fotos.push({
            id: data.rows.item(i).id,
            id_tblsalida: data.rows.item(i).id_tblsalida,
            img: '',
            ctitulo: data.rows.item(i).ctitulo,
            cobservacion: data.rows.item(i).cobservacion,
            zone: data.rows.item(i).zone,
            longitud: data.rows.item(i).longitud,
            latitud: data.rows.item(i).latitud,

          });
        }
      }
      this.fotos.next(fotos);
  }

  async loadFotosById(idTabla) {

    // code goes here

     const data = await this.database.executeSql('SELECT * FROM tblfoto WHERE id_tblsalida=' + idTabla, []);
     let fotos = [];
     if (data.rows.length > 0) {
       for (var i = 0; i < data.rows.length; i++) {
         /*let skills = [];
         if (data.rows.item(i).skills != '') {
           skills = JSON.parse(data.rows.item(i).skills);
         }*/
         fotos.push({
           id: data.rows.item(i).id,
           id_tblsalida: data.rows.item(i).id_tblsalida,
           img: data.rows.item(i).img,
           ctitulo: data.rows.item(i).ctitulo,
           cobservacion: data.rows.item(i).cobservacion,
           zone: data.rows.item(i).zone,
           longitud: data.rows.item(i).longitud,
           latitud: data.rows.item(i).latitud,

         });
       }
     }
     this.fotosById.next(fotos);
 }

  async loadFotosOnlyImage() {

      // code goes here

       const data = await this.database.executeSql('SELECT * FROM tblfoto', []);
       let fotos: any = [];
       if (data.rows.length > 0) {
         for (var i = 0; i < data.rows.length; i++) {
           /*let skills = [];
           if (data.rows.item(i).skills != '') {
             skills = JSON.parse(data.rows.item(i).skills);
           }*/
           fotos.push({
             id: data.rows.item(i).id,

             img: data.rows.item(i).img,

           });
         }
       }
       this.onlyFotos = fotos;
 }

 getNuevas() {
  //  this.loadNuevas();
   return this.nuevas;
 }
 async loadNuevas() {

      // code goes here
      console.log("Preparando para cargar...tblnuevo")
      const data = await this.database.executeSql('SELECT * FROM tblnuevo', []);
      console.log(data);
      let nuevas = [];
      if (data.rows.length > 0) {
        for (var i = 0; i < data.rows.length; i++) {
          /*let skills = [];
          if (data.rows.item(i).skills != '') {
            skills = JSON.parse(data.rows.item(i).skills);
          }*/
          nuevas.push({
            id_tblnuevo: data.rows.item(i).id_tblnuevo,
            idusuario: data.rows.item(i).idusuario,
            infraestructura: data.rows.item(i).infraestructura,
            subtipo: data.rows.item(i).subtipo,
            fecha_registro: data.rows.item(i).fecha_registro,
            longitud: data.rows.item(i).longitud,
            latitud: data.rows.item(i).latitud
          });
        }
      }
      this.nuevas.next(nuevas);
 }

 getNuevasFotos() {
   return this.nuevasFotos;
 }

async loadNuevasFotos() {

    // code goes here

     const data = await this.database.executeSql('SELECT * FROM tblfotonuevo', []);
     let fotos = [];
     if (data.rows.length > 0) {
       for (var i = 0; i < data.rows.length; i++) {
         /*let skills = [];
         if (data.rows.item(i).skills != '') {
           skills = JSON.parse(data.rows.item(i).skills);
         }*/
         fotos.push({
           id: data.rows.item(i).id,
           id_tblnuevo: data.rows.item(i).id_tblnuevo,
           img: '',
           ctitulo: data.rows.item(i).ctitulo,
           cobservacion: data.rows.item(i).cobservacion

         });
       }
     }
     this.nuevasFotos.next(fotos);

 }

 async loadNuevasFotosById(idTblNuevo) {
  const data = await this.database.executeSql('SELECT * FROM tblfotonuevo WHERE id_tblnuevo=' + idTblNuevo, []);
  console.log(data);
  let fotos = [];
  if (data.rows.length > 0) {
    for (var i = 0; i < data.rows.length; i++) {
      /*let skills = [];
      if (data.rows.item(i).skills != '') {
        skills = JSON.parse(data.rows.item(i).skills);
      }*/
      fotos.push({
        id: data.rows.item(i).id,
        id_tblnuevo: data.rows.item(i).id_tblnuevo,
        img: data.rows.item(i).img,
        ctitulo: data.rows.item(i).ctitulo,
        cobservacion: data.rows.item(i).cobservacion

      });
    }
  }
  this.nuevasFotosByid.next(fotos);
 }

 async loadFotosOnlyImageNuevo() {

     // code goes here

      const data = await this.database.executeSql('SELECT * FROM tblfotonuevo', []);
      let fotos: any = [];
      if (data.rows.length > 0) {
        for (var i = 0; i < data.rows.length; i++) {
          /*let skills = [];
          if (data.rows.item(i).skills != '') {
            skills = JSON.parse(data.rows.item(i).skills);
          }*/
          fotos.push({
            id: data.rows.item(i).id,

            img: data.rows.item(i).img,

          });
        }
      }
      this.onlyNuevosFotos = fotos;
  }


  async addSalida(codigo:any, idusuario:any, tipo: any, fotos:any ) {
      let fecha_registro = this.getFechaActual();
      this.generateId();
      let id_tblsalida = this.generate();


      let data = [id_tblsalida, codigo, idusuario, fecha_registro, tipo];
      console.log(data);
        //console.log(fotos);
      return this.database.executeSql('INSERT INTO tblsalida (id_tblsalida, codigo, idusuario, fecha_registro, tipo) VALUES (?, ?, ?, ?, ?)', data ).then(res => {
          this.addSalidaFoto(id_tblsalida, fotos);
        });
  }

  async addSalidaFoto(id_tblsalida: any, fotos: any) {

      this.generateId();
      fotos.forEach(async obj => {

         /*Object.entries(obj).forEach(([key, value]) => {
             console.log(`${key} ${value}`);
         });*/

         // let compressedImg = obj.img.split('').reduce((o, c) => {
         //    if (o[o.length - 2] === c && o[o.length - 1] < 35) o[o.length - 1]++;
         //    else o.push(c, 0);
         //    return o;
         //  },[]).map(_ => typeof _ === 'number' ? _.toString(36) : _).join('');
         // //console.log(compressedImg);
         let id = this.generate();
         let data = [id, id_tblsalida, obj.img, obj.ctitulo, obj.cobservacion, obj.zone, obj.longitud, obj.latitud ];
         console.log(data);
         this.database.executeSql('INSERT INTO tblfoto(id, id_tblsalida, img, ctitulo, cobservacion, zone, longitud, latitud  ) VALUES (?, ?, ?, ?, ?, ?, ?, ?)', data).then(res => {

         });
      });
  }

  async addNuevo(datos:any, idusuario:any, fotos:any ) {
      let fecha_registro = this.getFechaActual();
      this.generateId();
      let id_tblnuevo = this.generate();


      let data = [id_tblnuevo, idusuario, datos.infraestructura, datos.subtipo, fecha_registro, datos.longitud, datos.latitud];
      console.log(data);
        //console.log(fotos);
      return this.database.executeSql('INSERT INTO tblnuevo (id_tblnuevo, idusuario, infraestructura, subtipo, fecha_registro, longitud, latitud) VALUES (?, ?, ?, ?, ?, ?, ?)', data ).then(res => {
          this.addNuevoFoto(id_tblnuevo, fotos);
        });
  }

  async addNuevoFoto(id_tblnuevo: any, fotos: any) {

      this.generateId();
      fotos.forEach(async obj => {

         /*Object.entries(obj).forEach(([key, value]) => {
             console.log(`${key} ${value}`);
         });*/

         let id = this.generate();
         let data = [id, id_tblnuevo, obj.img, obj.ctitulo, obj.cobservacion ];
         console.log(data);
         this.database.executeSql('INSERT INTO tblfotonuevo(id, id_tblnuevo, img, ctitulo, cobservacion) VALUES (?, ?, ?, ?, ?)', data).then(res => {

         });
      });
  }


 async deleteSalida() {
   return this.database.executeSql('DELETE FROM tblsalida').then(_ => {
     this.deleteSalidaFoto();
   });
 }

 async deleteSalidaFoto() {
   return this.database.executeSql('DELETE FROM tblfoto').then(_ => {
   });
 }

async deleteNuevo() {
   return this.database.executeSql('DELETE FROM tblnuevo').then(_ => {
     this.deleteNuevoFoto();
   });
 }

 async deleteNuevoFoto() {
   const _ = await this.database.executeSql('DELETE FROM tblfotonuevo');
 }




  compressedImage(img:any){

    let compressedImg = img.split('').reduce((o, c) => {
      if (o[o.length - 2] === c && o[o.length - 1] < 35) o[o.length - 1]++;
      else o.push(c, 0);
      return o;
    },[]).map(_ => typeof _ === 'number' ? _.toString(36) : _).join('');

  }


  generateId(){
    var length = 15;
    var timestamp = +new Date;
    var _getRandomInt = function( min, max ) {
      return Math.floor( Math.random() * ( max - min + 1 ) ) + min;
    };
    this.generate = function() {
      var ts = timestamp.toString();
      var parts = ts.split( "" ).reverse();
      var id = "";
      for( var i = 0; i < length; ++i ) {
         var index = _getRandomInt( 0, parts.length - 1 );
         id += parts[index];
      }
      return id;
    };
  }

  getFechaActual(){
      var today = new Date();
      var dd = String(today.getDate()).padStart(2, '0');
      var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
      var yyyy = today.getFullYear();
      var fecha = dd + '-' + mm + '-' + yyyy;
      return fecha;
  }

}
