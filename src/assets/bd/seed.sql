
CREATE TABLE IF NOT EXISTS tblsalida(id_tblsalida TEXT PRIMARY KEY, codigo TEXT, idusuario TEXT, fecha_registro TEXT, tipo TEXT);

CREATE TABLE IF NOT EXISTS tblfoto(id TEXT PRIMARY KEY,id_tblsalida TEXT, img BLOB, ctitulo TEXT, cobservacion TEXT,zone TEXT, longitud, TEXT, latitud TEXT);

CREATE TABLE IF NOT EXISTS tblnuevo(id_tblnuevo TEXT PRIMARY KEY, idusuario TEXT, infraestructura TEXT, subtipo TEXT, fecha_registro TEXT, longitud, TEXT, latitud TEXT);

CREATE TABLE IF NOT EXISTS tblfotonuevo(id TEXT PRIMARY KEY, id_tblnuevo TEXT, img BLOB, ctitulo TEXT, cobservacion TEXT);
